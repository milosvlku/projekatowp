package com.ftn.ProjekatOWP.services;

import com.ftn.ProjekatOWP.models.Korpa;

import java.util.UUID;

public interface KorpaService {

    public Korpa findOne(UUID idKorpe);

    public Korpa findOneByKorisnikIdKorisnika(UUID idKorisnika);

    public int save(Korpa korpa);

    public int update(Korpa korpa);

    public int delete(UUID idKorpe);



}
