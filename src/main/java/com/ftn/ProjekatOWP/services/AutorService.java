package com.ftn.ProjekatOWP.services;

import com.ftn.ProjekatOWP.models.Autor;

import java.util.List;
import java.util.UUID;

public interface AutorService {

    public Autor findOne(UUID idAutora);

    public List<Autor> findAll();

    public int save(Autor autor);

    public int update(Autor autor);

    public int delete(UUID idAutora);

    public List<Autor> parsirajAutoreIzStringa(String string) throws Exception;



}
