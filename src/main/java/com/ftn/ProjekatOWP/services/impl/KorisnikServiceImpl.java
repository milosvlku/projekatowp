package com.ftn.ProjekatOWP.services.impl;

import com.ftn.ProjekatOWP.dao.KorisnikDAO;
import com.ftn.ProjekatOWP.enums.Uloga;
import com.ftn.ProjekatOWP.models.Korisnik;
import com.ftn.ProjekatOWP.services.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
public class KorisnikServiceImpl implements KorisnikService {

    @Autowired
    private KorisnikDAO korisnikDAO;

    @Override
    public Korisnik findOne(UUID idKorisnika) {
        return korisnikDAO.findOne(idKorisnika);
    }

    @Override
    public Korisnik findOneByUsername(String korisnickoIme) {
        return korisnikDAO.findOne(korisnickoIme);
    }

    @Override
    public Korisnik findOne(String korisnickoIme, String lozinka) {
        return korisnikDAO.findOne(korisnickoIme, lozinka);
    }

    @Override
    public List<Korisnik> findAll() {
        return korisnikDAO.findAll();
    }

    @Override
    public List<Korisnik> find(String korisnickoIme, Uloga uloga) {
        return korisnikDAO.find(korisnickoIme, uloga);
    }

    @Override
    public int save(Korisnik korisnik) {
        return korisnikDAO.save(korisnik);
    }

    @Override
    public int update(Korisnik korisnik) {
        return korisnikDAO.update(korisnik);
    }

    @Override
    public int delete(UUID idKorisnika) {
        return korisnikDAO.delete(idKorisnika);
    }

    @Override
    public String proveriIspravnostPodataka(String korisnickoIme, String lozinka, String ponovljenaLozinka, String email, String ime, String prezime, LocalDate datumRodjenja, String adresa, String brojTelefona) {
        String greska = "";

        System.out.println("Korisnicko je " +korisnickoIme);
        if(korisnickoIme.equals("") || korisnickoIme == null) {
            greska += "neispravno korisničko ime ";
        }

        if(lozinka.equals("") || lozinka == null) {
            greska += "neispravna lozinka ";
        }

        if(lozinka.equals("") || lozinka == null) {
            greska += "neispravna ponovljena lozinka ";
        } else if(!lozinka.equals(ponovljenaLozinka)) {
            greska += "lozinke se ne poklapaju! ";
        }

        if(email.equals("") || email == null) {
            greska += "neispravan email ";
        }

        if(ime.equals("") || ime == null) {
            greska += " neispravno ime";
        }

        if(prezime.equals("") || prezime == null) {
            greska += " neispravno prezime";
        }

        if(datumRodjenja == null) {
            greska += " neispravan datum rodjenja";
        } else if(datumRodjenja.compareTo(LocalDate.now()) > 0) {
            greska += " neispravan datum rodjenja";
        }

        if(adresa.equals("") || adresa == null) {
            greska += " neispravna adresa";
        }

        if(brojTelefona.equals("") || brojTelefona == null) {
            greska += " neispravan broj telefona";
        }


        return greska;
    }

    @Override
    public void ocistiSesiju(HttpSession sesija) {
        sesija.removeAttribute("korisnicko");
        sesija.removeAttribute("password");
        sesija.removeAttribute("email");
        sesija.removeAttribute("ime");
        sesija.removeAttribute("prezime");
        sesija.removeAttribute("datumRodjenja");
        sesija.removeAttribute("adresa");
        sesija.removeAttribute("brojTelefona");
    }
}
