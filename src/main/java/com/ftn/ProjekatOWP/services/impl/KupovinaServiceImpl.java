package com.ftn.ProjekatOWP.services.impl;

import com.ftn.ProjekatOWP.dao.KupljenaKnjigaDAO;
import com.ftn.ProjekatOWP.dao.KupovinaDAO;
import com.ftn.ProjekatOWP.models.KorpaKnjiga;
import com.ftn.ProjekatOWP.models.KupljenaKnjiga;
import com.ftn.ProjekatOWP.models.Kupovina;
import com.ftn.ProjekatOWP.services.KupovinaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class KupovinaServiceImpl implements KupovinaService {

    @Autowired
    private KupovinaDAO kupovinaDAO;

    @Autowired
    private KupljenaKnjigaDAO kupljenaKnjigaDAO;

    @Override
    public Kupovina findOne(UUID idKupovine) {
        Kupovina kupovina = kupovinaDAO.findOne(idKupovine);
        kupovina.setKupovinaOdredjeneKnjige(kupljenaKnjigaDAO.findAllByKupovinaIdKupovine(idKupovine));
        double ukupno = 0;
        for(KupljenaKnjiga kupljenaKnjiga:kupovina.getKupovinaOdredjeneKnjige()) {
            ukupno += (kupljenaKnjiga.getKnjiga().getCena() * kupljenaKnjiga.getBrojPrimeraka()) - (kupljenaKnjiga.getKnjiga().getCena() * kupljenaKnjiga.getBrojPrimeraka()) * (0.05 * kupovina.getBrojUtrosenihPoena());
        }
        kupovina.setUkupnaCenaKupovine(ukupno);
        return kupovina;
    }

    @Override
    public List<Kupovina> findByKorisnikIdKorisnika(UUID idKorisnika) {
        List<Kupovina> kupovine = kupovinaDAO.findByKorisnikIdKorisnika(idKorisnika);
        for(Kupovina kupovina:kupovine) {
            kupovina.setKupovinaOdredjeneKnjige(kupljenaKnjigaDAO.findAllByKupovinaIdKupovine(kupovina.getIdKupovine()));
            double ukupno = 0;
            for(KupljenaKnjiga kupljenaKnjiga:kupovina.getKupovinaOdredjeneKnjige()) {
                ukupno += (kupljenaKnjiga.getKnjiga().getCena() * kupljenaKnjiga.getBrojPrimeraka()) - (kupljenaKnjiga.getKnjiga().getCena() * kupljenaKnjiga.getBrojPrimeraka()) * (0.05 * kupovina.getBrojUtrosenihPoena());
            }
            kupovina.setUkupnaCenaKupovine(ukupno);
        }
        return kupovine;
    }



    @Override
    public int save(Kupovina kupovina) {
        kupovinaDAO.save(kupovina);
        int counter = 0;
        for(KupljenaKnjiga kupljenaKnjiga:kupovina.getKupovinaOdredjeneKnjige()) {
            kupljenaKnjigaDAO.save(kupljenaKnjiga);
            counter++;
        }

        return counter;
    }

    @Override
    public int update(Kupovina kupovina) {
        return 0;
    }

    @Override
    public int delete(UUID idKorpe) {
        return 0;
    }
}
