package com.ftn.ProjekatOWP.services.impl;

import com.ftn.ProjekatOWP.dao.ListaZeljaDAO;
import com.ftn.ProjekatOWP.dao.ListaZeljaKnjigaDAO;
import com.ftn.ProjekatOWP.models.Knjiga;
import com.ftn.ProjekatOWP.models.Korpa;
import com.ftn.ProjekatOWP.models.ListaZelja;
import com.ftn.ProjekatOWP.models.ListaZeljaKnjiga;
import com.ftn.ProjekatOWP.services.KnjigaService;
import com.ftn.ProjekatOWP.services.ListaZeljaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.UUID;

@Service
public class ListaZeljaServiceImpl implements ListaZeljaService {

    @Autowired
    private ListaZeljaDAO listaZeljaDAO;

    @Autowired
    private KnjigaService knjigaService;

    @Autowired
    private ListaZeljaKnjigaDAO listaZeljaKnjigaDAO;

    @Override
    public ListaZelja findOne(UUID idListeZelja) {
        ListaZelja listaZelja = listaZeljaDAO.findOne(idListeZelja);
        Set<ListaZeljaKnjiga> listaZeljaKnjigaSet = listaZeljaKnjigaDAO.findByListaZeljaIdListeZelja(idListeZelja);
        listaZeljaKnjigaSet.stream().forEach(listaZeljaKnjiga -> listaZelja.getKnjige().add(listaZeljaKnjiga.getKnjiga()));
        return listaZelja;
    }

    @Override
    public ListaZelja findOneByKorisnikIdKorisnika(UUID idKorisnika) {
        ListaZelja listaZelja = listaZeljaDAO.findListaZeljaByKorisnikIdKorisnika(idKorisnika);
        if(listaZelja != null) {
            listaZeljaKnjigaDAO.findByListaZeljaIdListeZelja(listaZelja.getIdListeZelja())
                    .stream().forEach(listaZeljaKnjiga -> listaZelja.getKnjige().add(listaZeljaKnjiga.getKnjiga()));
        }
        return listaZelja;
    }

    @Override
    public int save(ListaZelja listaZelja) {
        listaZeljaDAO.save(listaZelja);
        int counter = 0;
        for(Knjiga knjiga:listaZelja.getKnjige()) {
            listaZeljaKnjigaDAO.save(new ListaZeljaKnjiga(UUID.randomUUID(), listaZelja, knjiga));
            counter++;
        }
        return counter;
    }

    @Override
    public int update(ListaZelja listaZelja) {
        return listaZeljaDAO.update(listaZelja);
    }

    @Override
    public int delete(UUID idListeZelja) {
        return listaZeljaDAO.delete(idListeZelja);
    }
}
