package com.ftn.ProjekatOWP.services.impl;




import com.ftn.ProjekatOWP.dao.*;
import com.ftn.ProjekatOWP.models.*;
import com.ftn.ProjekatOWP.services.KnjigaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class KnjigaServiceImpl implements KnjigaService {

    @Autowired
    private KnjigaDAO knjigaDAO;

    @Autowired
    private KnjigaAutorDAO knjigaAutorDAO;

    @Autowired
    private KnjigaZanrDAO knjigaZanrDAO;

    @Autowired
    private KomentarDAO komentarDAO;

    @Override
    public Knjiga findOne(UUID idKnjige) {

        Knjiga knjiga = knjigaDAO.findOne(idKnjige);
        List<KnjigaAutor> knjigaAutori = knjigaAutorDAO.findByKnjiga(idKnjige);
        List<KnjigaZanr> knjigaZanrovi = knjigaZanrDAO.findByKnjiga(idKnjige);


        for(KnjigaAutor ka : knjigaAutori) {
            knjiga.getAutori().add(ka.getAutor());
        }

        for(KnjigaZanr kz : knjigaZanrovi) {
            knjiga.getZanrovi().add(kz.getZanr());
        }

        dodajOcenuKnjizi(knjiga);

        return knjiga;
    }

    //Ovo radimo da bi smo spojili sirove knjige sa autorima, zanrovima i prosecnom ocenama
    @Override
    public List<Knjiga> findAll() {

        List<Knjiga> knjigeTemp = knjigaDAO.findAll();
        List<Knjiga> knjige = new ArrayList<>();

        for(Knjiga k : knjigeTemp) {
            knjige.add(findOne(k.getIdKnjige()));
        }

        return knjige;
    }

    @Override
    public int save(Knjiga knjiga) {
        knjigaDAO.save(knjiga);
        int counter = 0;
        for(Zanr z:knjiga.getZanrovi()) {
            try {
                knjigaZanrDAO.save(knjigaZanrDAO.findByKnjigaZanr(knjiga.getIdKnjige(), z.getIdZanra()));
                counter++;
            } catch (Exception ex) {
                System.out.println("Desila se greska!, u save kod knjige service, hendlujemo");
                ex.printStackTrace();
                knjigaZanrDAO.save(new KnjigaZanr(UUID.randomUUID(), knjiga, z));
                counter++;
            }
        }

        for(Autor a:knjiga.getAutori()) {
            try {
                knjigaAutorDAO.save(knjigaAutorDAO.findByKnjigaAutor(knjiga.getIdKnjige(), a.getIdAutora()));
                counter++;
            } catch (Exception ex) {
                System.out.println("Desila se greska!, u save kod knjige service, hendlujemo");
                ex.printStackTrace();
                knjigaAutorDAO.save(new KnjigaAutor(UUID.randomUUID(), knjiga, a));
                counter++;
            }
        }

        return counter;
    }

    @Override
    public int update(Knjiga knjiga) {
        return knjigaDAO.update(knjiga);
    }

    @Override
    public int delete(UUID idKnjige) {
        return knjigaDAO.delete(idKnjige);
    }

    @Override
    public List<Knjiga> find(String naziv, String ISBN, Double cenaOd, Double cenaDo, String jezik, Zanr zanr, Autor autor, String sortiranje) {

        //Pretvara sve prazne vrednosti u null da bi korektno proslo u DAO/SERVICE sloju.
        if(naziv != null) {
            if (naziv.equals("")) {
                naziv = null;
            }
        }

        if(ISBN != null) {
            if (ISBN.equals("")) {
                ISBN = null;
            }
        }

        if(jezik != null) {
            if (jezik.equals("")) {
                jezik = null;
            }
        }

        if(autor != null) {
            if (autor.equals("")) {
                autor = null;
            }
        }

        if(sortiranje != null) {
            if (sortiranje.equals("")) {
                sortiranje = null;
            }
        }

        List<Knjiga> knjigeTemp = knjigaDAO.find(naziv,ISBN,cenaOd,cenaDo,jezik);
        List<Knjiga> trazeneKnjige = new ArrayList<>();

        //Ovo radimo da bi knjigama dodali zanrove i autore.
        for(Knjiga k : knjigeTemp) {
            trazeneKnjige.add(findOne(k.getIdKnjige()));
        }
        List<Knjiga> neOdgovarajuceKnjige = new ArrayList<>();

        System.out.println("Zanr " + zanr + " " + " Autor " +autor);
        for(Knjiga knjiga : trazeneKnjige) {
            if(zanr != null) {
                if(!knjiga.getZanrovi().contains(zanr)) {
                    neOdgovarajuceKnjige.add(knjiga);
                }
            }
        }

        for(Knjiga knjiga : trazeneKnjige) {
            if(autor != null) {
                if(!knjiga.getAutori().contains(autor)) {
                    neOdgovarajuceKnjige.add(knjiga);
                }
            }
        }



        for(Knjiga knjiga : neOdgovarajuceKnjige) {
            trazeneKnjige.remove(knjiga);
        }

        System.out.println("Sortiranje " + sortiranje);

        if(sortiranje != null) {
            switch (sortiranje) {
                case "nazivRastuci":
                    trazeneKnjige.sort(sortiranjeNaziv);
                    break;
                case "nazivOpadajuci":
                    trazeneKnjige.sort(sortiranjeNaziv.reversed());
                    break;
                case "cenaRastuci":
                    trazeneKnjige.sort(sortiranjeCena);
                    break;
                case "cenaOpadajuci":
                    trazeneKnjige.sort(sortiranjeCena.reversed());
                    break;
                case "jezikRastuci":
                    trazeneKnjige.sort(sortiranjeJezik);
                    break;
                case "jezikOpadajuci":
                    trazeneKnjige.sort(sortiranjeJezik.reversed());
                    break;
                case "ocenaRastuci":
                    trazeneKnjige.sort(sortiranjeOcena);
                    break;
                case "ocenaOpadajuci":
                    trazeneKnjige.sort(sortiranjeOcena.reversed());
                    break;
                default:
                    System.out.println("Default");
            }
        }

        return trazeneKnjige;
    }

    private void dodajOcenuKnjizi(Knjiga knjiga) {
        double brojOcena = 0;
        double zbirOcena = 0;

        for(Komentar kom:komentarDAO.findByKnjiga(knjiga.getIdKnjige())) {
            brojOcena += 1;
            zbirOcena += kom.getOcena();
        }

        //YOLO zaokruzio sam ga jer mi je lakse posle da kreiram mrtve zvezdice.
        knjiga.setOcena((int) Math.round(zbirOcena / brojOcena));
    }

     public static String ispisiAutoreKnjige(Knjiga knjiga) {
        StringBuffer sb = new StringBuffer();
        for(Autor autor:knjiga.getAutori()) {
            sb.append(autor.getImeAutora() + " " + autor.getPrezimeAutora() + ";");
        }
        return sb.toString();
    }



    Comparator<Knjiga> sortiranjeNaziv = new Comparator<Knjiga>() {
        @Override
        public int compare(Knjiga o1, Knjiga o2) {
            return o1.getNaziv().compareTo(o2.getNaziv());
        }
    };

    Comparator<Knjiga> sortiranjeJezik = new Comparator<Knjiga>() {
        @Override
        public int compare(Knjiga o1, Knjiga o2) { return o1.getJezik().compareTo(o2.getJezik()); }
    };

    Comparator<Knjiga> sortiranjeCena = new Comparator<Knjiga>() {
        @Override
        public int compare(Knjiga o1, Knjiga o2) {
            if(o1.getCena() > o2.getCena())
                return 1;
            else
                return -1;
        }
    };

    Comparator<Knjiga> sortiranjeOcena = new Comparator<Knjiga>() {
        @Override
        public int compare(Knjiga o1, Knjiga o2) {
            if(o1.getOcena() > o2.getOcena())
                return 1;
            else
                return -1;
        }
    };

}
