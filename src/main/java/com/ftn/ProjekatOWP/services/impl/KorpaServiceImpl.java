package com.ftn.ProjekatOWP.services.impl;

import com.ftn.ProjekatOWP.dao.KorpaDAO;
import com.ftn.ProjekatOWP.dao.KorpaKnjigaDAO;
import com.ftn.ProjekatOWP.models.Korpa;
import com.ftn.ProjekatOWP.models.KorpaKnjiga;
import com.ftn.ProjekatOWP.services.KorpaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class KorpaServiceImpl implements KorpaService {

    @Autowired
    private KorpaDAO korpaDAO;

    @Autowired
    private KorpaKnjigaDAO korpaKnjigaDAO;

    @Override
    public Korpa findOne(UUID idKorpe) {
        Korpa korpa = korpaDAO.findOne(idKorpe);
        korpa.setStavke(korpaKnjigaDAO.findByKnjigaIdKnjige(korpa.getIdKorpe()));
        return korpa;
    }

    @Override
    public Korpa findOneByKorisnikIdKorisnika(UUID idKorisnika) {
        Korpa korpa = korpaDAO.findKorpaByKorisnikIdKorisnika(idKorisnika);
        if(korpa != null) {
            korpa.setStavke(korpaKnjigaDAO.findByKorpaIdKorpe(korpa.getIdKorpe()));
        }
        return korpa;
    }

    @Transactional
    @Override
    public int save(Korpa korpa) {
        korpaDAO.save(korpa);
        int counter = 0;
        for(KorpaKnjiga korpaKnjiga:korpa.getStavke()) {
            korpaKnjigaDAO.save(korpaKnjiga);
            counter++;
        }
        return counter;
    }

    @Transactional
    @Override
    public int update(Korpa korpa) {
        return korpaDAO.update(korpa);
    }

    @Transactional
    @Override
    public int delete(UUID idKorpe) {
        Korpa korpa = findOne(idKorpe);
        int counter = 0;
        for(KorpaKnjiga korpaKnjiga:korpa.getStavke()) {
            counter += korpaKnjigaDAO.delete(korpaKnjiga.getIdKorpeKnjige());
        }
        counter += korpaDAO.delete(idKorpe);
        return counter;
    }
}
