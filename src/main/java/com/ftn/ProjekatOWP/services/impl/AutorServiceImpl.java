package com.ftn.ProjekatOWP.services.impl;

import com.ftn.ProjekatOWP.dao.AutorDAO;
import com.ftn.ProjekatOWP.models.Autor;
import com.ftn.ProjekatOWP.models.Knjiga;
import com.ftn.ProjekatOWP.services.AutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class AutorServiceImpl implements AutorService {

    @Autowired
    AutorDAO autorDAO;

    @Override
    public Autor findOne(UUID idAutora) {
        return autorDAO.findOne(idAutora);
    }

    @Override
    public List<Autor> findAll() {
        return autorDAO.findAll();
    }

    @Override
    public int save(Autor autor) {
        return autorDAO.save(autor);
    }

    @Override
    public int update(Autor autor) {
        return autorDAO.update(autor);
    }

    @Override
    public int delete(UUID idAutora) {
        return autorDAO.delete(idAutora);
    }

    @Override
    public List<Autor> parsirajAutoreIzStringa(String string) throws Exception {
        List<Autor> autori = new ArrayList<>();

        for(String s : string.trim().split(";")) {
            System.out.println(s);
        }

        try {
            List<String> ip = new ArrayList<>();

            for(String imePrezime : string.trim().split(";")) {
                ip.clear();
                if (imePrezime.trim() != "") {
                    for(String s:imePrezime.split(" ")) {
                        if(s != "") {
                            ip.add(s);
                        } else {
                            throw new Exception();
                        }
                    }
                } else {
                    throw new Exception();
                }
                autori.add(autorDAO.findOneByImePrezime(ip.get(0), ip.get(1)));
            }
        } catch (Exception ex) {
            throw ex;
        }

        return autori;
    }
}
