package com.ftn.ProjekatOWP.services;

import com.ftn.ProjekatOWP.models.ListaZelja;

import java.util.UUID;

public interface ListaZeljaService {

    public ListaZelja findOne(UUID idListeZelja);

    public ListaZelja findOneByKorisnikIdKorisnika(UUID idKorisnika);

    public int save(ListaZelja listaZelja);

    public int update(ListaZelja listaZelja);

    public int delete(UUID idListeZelja);

}
