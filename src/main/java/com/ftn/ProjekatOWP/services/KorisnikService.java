package com.ftn.ProjekatOWP.services;

import com.ftn.ProjekatOWP.enums.Uloga;
import com.ftn.ProjekatOWP.models.Korisnik;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface KorisnikService {

    public Korisnik findOne(UUID idKorisnika);

    public Korisnik findOneByUsername(String username);

    public Korisnik findOne(String korisnickoIme, String lozinka);

    public List<Korisnik> findAll();

    public List<Korisnik> find(String korisnickoIme, Uloga uloga);

    public int save(Korisnik korisnik);

    public int update(Korisnik korisnik);

    public int delete(UUID idKorisnika);

    public String proveriIspravnostPodataka(String korisnickoIme, String lozinka, String ponovljenaLozinka, String email, String ime, String prezime, LocalDate datumRodjenja, String adresa, String brojTelefona);

    public void ocistiSesiju(HttpSession sesija);
}
