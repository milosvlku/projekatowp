package com.ftn.ProjekatOWP.services;

import com.ftn.ProjekatOWP.models.Autor;
import com.ftn.ProjekatOWP.models.Knjiga;
import com.ftn.ProjekatOWP.models.Zanr;

import java.util.List;
import java.util.UUID;

public interface KnjigaService {
    public Knjiga findOne(UUID idKnjige);

    public List<Knjiga> findAll();

    public int save(Knjiga knjiga);

    public int update(Knjiga knjiga);

    public int delete(UUID idKnjige);

    public List<Knjiga> find(String naziv, String ISBN, Double cenaOd, Double cenaDo, String jezik, Zanr zanr, Autor autor, String sortiranje);
}
