package com.ftn.ProjekatOWP.services;

import com.ftn.ProjekatOWP.models.Korpa;
import com.ftn.ProjekatOWP.models.Kupovina;

import java.util.List;
import java.util.UUID;

public interface KupovinaService {

    public Kupovina findOne(UUID idKupovine);

    public List<Kupovina> findByKorisnikIdKorisnika(UUID idKorisnika);

    public int save(Kupovina kupovina);

    public int update(Kupovina kupovina);

    public int delete(UUID idKorpe);

}
