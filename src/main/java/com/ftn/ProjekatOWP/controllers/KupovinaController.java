package com.ftn.ProjekatOWP.controllers;

import com.ftn.ProjekatOWP.dao.KorpaKnjigaDAO;
import com.ftn.ProjekatOWP.dao.KupovinaDAO;
import com.ftn.ProjekatOWP.dao.LoyaltyKarticaDAO;
import com.ftn.ProjekatOWP.enums.Status;
import com.ftn.ProjekatOWP.enums.Uloga;
import com.ftn.ProjekatOWP.models.*;
import com.ftn.ProjekatOWP.services.KnjigaService;
import com.ftn.ProjekatOWP.services.KorpaService;
import com.ftn.ProjekatOWP.services.KupovinaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.UUID;

@Controller
@RequestMapping(value = "/Kupovina")
public class KupovinaController {

    public static final String KORISNIK_KEY = "prijavljeniKorisnik";

    @Autowired
    private KorpaService korpaService;

    @Autowired
    private KnjigaService knjigaService;

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private KorpaKnjigaDAO korpaKnjigaDAO;

    @Autowired
    private LoyaltyKarticaDAO loyaltyKarticaDAO;

    @Autowired
    private KupovinaService kupovinaService;

    private String baseURL;

    private Korisnik prijavljeniKorisnik;

    @PostConstruct
    public void init() { baseURL = servletContext.getContextPath() + "/"; }

    @PostMapping("/Kupi")
    public void kupi(@RequestParam(required = true) int brojPoena,
                     HttpSession session, HttpServletResponse response) throws IOException {
        prijavljeniKorisnik = (Korisnik) session.getAttribute(KORISNIK_KEY);
        LoyaltyKartica loyaltyKartica = null;

        if(prijavljeniKorisnik == null || korpaService.findOneByKorisnikIdKorisnika(prijavljeniKorisnik.getIdKorisnika()) == null || prijavljeniKorisnik.getUloga() != Uloga.KUPAC) {
            response.sendRedirect(baseURL + "/");
            return;
        }


        loyaltyKartica = loyaltyKarticaDAO.findByKorisnik(prijavljeniKorisnik.getIdKorisnika());
        if(loyaltyKartica == null || loyaltyKartica.getStatusKartice().equals(Status.NA_CEKANJU) || loyaltyKartica.getStatusKartice().equals(Status.NIJE_ODOBREN)) {
            if(brojPoena > 0) {
                System.out.println("Kartica ne postoji/nije odobrena/na cekanju te ne mozemo trositi poene.");
                response.sendRedirect(baseURL + "/");
                return;
            }
        } else if(loyaltyKartica.getBrojPoena() < brojPoena) {
            System.out.println("Na kartici ima manje poena nego sto pokusavamo da potrosimo");
            response.sendRedirect(baseURL + "/");
            return;
        }





        Korpa korpa = korpaService.findOneByKorisnikIdKorisnika(prijavljeniKorisnik.getIdKorisnika());

        //TODO: Ovde voditi racuna o kreiranim popustima za implementaciju
        Kupovina kupovina = new Kupovina(UUID.randomUUID(),LocalDate.now(),
                prijavljeniKorisnik, brojPoena);



        for(KorpaKnjiga korpaKnjiga:korpa.getStavke()) {
            kupovina.getKupovinaOdredjeneKnjige().add(
                    new KupljenaKnjiga(UUID.randomUUID(), kupovina,
                            korpaKnjiga.getKnjiga(), korpaKnjiga.getKolicina()));
        }

        int rez = kupovinaService.save(kupovina);


        if(rez == 0) {
            System.out.println("Kupovina nije uspela");
        } else {
            System.out.println("Kupovina uspesna");
            loyaltyKartica.setBrojPoena(loyaltyKartica.getBrojPoena() - brojPoena);
            loyaltyKarticaDAO.update(loyaltyKartica);
            dodajKorisnikuPoene(kupovina.getIdKupovine());


            korpa.setStavke(new ArrayList<>());
            korpaService.update(korpa);
        }

        response.sendRedirect(baseURL + "Korpa");
    }

    private void dodajKorisnikuPoene(UUID uuidKupovine) {
        Kupovina kupovina = kupovinaService.findOne(uuidKupovine);
        double potroseno = kupovina.getUkupnaCenaKupovine();
        int poeni = ((int)Math.round(potroseno)) / 1000;
        LoyaltyKartica loyaltyKartica = loyaltyKarticaDAO.
                findByKorisnik(prijavljeniKorisnik.getIdKorisnika());
        if(loyaltyKartica == null || loyaltyKartica.getStatusKartice().equals(Status.NA_CEKANJU) || loyaltyKartica.getStatusKartice().equals(Status.NIJE_ODOBREN)) {

        } else {
            poeni += loyaltyKartica.getBrojPoena();
            if(poeni >= 10) {
                loyaltyKartica.setBrojPoena(10);
            } else {
                loyaltyKartica.setBrojPoena(poeni);
            }
            loyaltyKarticaDAO.update(loyaltyKartica);
        }
    }



}
