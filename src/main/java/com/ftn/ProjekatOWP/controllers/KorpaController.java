package com.ftn.ProjekatOWP.controllers;

import com.ftn.ProjekatOWP.dao.KorpaKnjigaDAO;
import com.ftn.ProjekatOWP.dao.LoyaltyKarticaDAO;
import com.ftn.ProjekatOWP.enums.Uloga;
import com.ftn.ProjekatOWP.models.*;
import com.ftn.ProjekatOWP.services.KnjigaService;
import com.ftn.ProjekatOWP.services.KorpaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.session.Session;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.yaml.snakeyaml.events.Event;

import javax.annotation.PostConstruct;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.UUID;

@Controller
@RequestMapping(value = "/Korpa")
public class KorpaController {
    public static final String KORISNIK_KEY = "prijavljeniKorisnik";

    @Autowired
    private KorpaService korpaService;

    @Autowired
    private KnjigaService knjigaService;

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private KorpaKnjigaDAO korpaKnjigaDAO;

    @Autowired
    private LoyaltyKarticaDAO loyaltyKarticaDAO;

    private String baseURL;

    @PostConstruct
    public void init() { baseURL = servletContext.getContextPath() + "/"; }

    @GetMapping()
    public ModelAndView korpa(HttpSession session, HttpServletResponse response) throws IOException {
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KORISNIK_KEY);
        System.out.println("Prijavljeni korisnik = " + prijavljeniKorisnik);
        // Proveravamo da li je korisnik uopste ulogovan i da li je korisnik kupac.
        // Ako jeste prikazujemo stranicu a ako nije vracamo na pocetnu.
        if(prijavljeniKorisnik != null && prijavljeniKorisnik.getUloga() == Uloga.KUPAC) {
            ModelAndView rezultat = new ModelAndView("Korpa");
            Korpa korisnickaKorpa = korpaService.findOneByKorisnikIdKorisnika(prijavljeniKorisnik.getIdKorisnika());
            rezultat.addObject("korpa", korisnickaKorpa);
            rezultat.addObject("kartica", loyaltyKarticaDAO.findByKorisnik(prijavljeniKorisnik.getIdKorisnika()));
            return rezultat;
        }

        response.sendRedirect(baseURL);
        return null;
    }

    @PostMapping(value = "/DodajUKorpu")
    public void dodajUKorpu(@RequestParam(required = true) String idKnjige,
                            @RequestParam(required = false) Integer brPrimeraka,
                            HttpSession session, HttpServletResponse response) throws IOException {
        System.out.println("Dodaj u horpu endpoint pogodjen " + idKnjige + " " + brPrimeraka);
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KORISNIK_KEY);
        Knjiga knjiga = knjigaService.findOne(UUID.fromString(idKnjige));
        Korpa korpa = korpaService.findOneByKorisnikIdKorisnika(prijavljeniKorisnik.getIdKorisnika());


        if(prijavljeniKorisnik == null || prijavljeniKorisnik.getUloga() != Uloga.KUPAC) {
            response.sendRedirect(baseURL + "/");
            return;
        }

        // TODO: Ovde bi realno mogao da posaljes povratnu poruku ili nesto.
        if(brPrimeraka == null || brPrimeraka > knjiga.getBrojPrimeraka()) {
            if(korpa == null) {
                korpa = new Korpa(UUID.randomUUID(), prijavljeniKorisnik);
            }
            System.out.println("Premalo knjiga");
            response.sendRedirect(baseURL + "/");
            return;
        }

        if(korpa == null) {
            System.out.println("Korisnik nema korpu, kreiramo.");
            korpa = new Korpa(UUID.randomUUID(), prijavljeniKorisnik);
            KorpaKnjiga korpaKnjiga = new KorpaKnjiga(UUID.randomUUID(), korpa, knjiga, brPrimeraka);
            korpa.getStavke().add(korpaKnjiga);
            int rez = korpaService.save(korpa);
            System.out.println(rez + " rez");
            if(rez != 0) {
                knjiga.setBrojPrimeraka(knjiga.getBrojPrimeraka() - brPrimeraka);
                knjigaService.update(knjiga);
            }
        } else {
            if(korpa.getStavke().stream().filter(korpaKnjiga -> korpaKnjiga.getKnjiga().getIdKnjige().equals(knjiga.getIdKnjige())).findFirst().isPresent()) {
                System.out.println("Stavka vec postoji, dodajemo na nju.");
                KorpaKnjiga korpaKnjiga = korpaKnjigaDAO.findByKorpaIdKorpeAndKnjigaIdKnjige(korpa.getIdKorpe(), knjiga.getIdKnjige());
                korpaKnjiga.setKolicina(korpaKnjiga.getKolicina() + brPrimeraka);
                int rez = korpaKnjigaDAO.update(korpaKnjiga);
                if(rez != 0) {
                    knjiga.setBrojPrimeraka(knjiga.getBrojPrimeraka() - brPrimeraka);
                    knjigaService.update(knjiga);
                }
            } else {
                System.out.println("Stavka nije postojala, dodajemo.");
                korpa.getStavke().add(new KorpaKnjiga(UUID.randomUUID(), korpa, knjiga, brPrimeraka));
                int rez = korpaService.update(korpa);
                if(rez != 0) {
                    knjiga.setBrojPrimeraka(knjiga.getBrojPrimeraka() - brPrimeraka);
                    knjigaService.update(knjiga);
                }
            }
        }
        response.sendRedirect(baseURL + "Korpa");
    }

    @PostMapping(value = "/IzbaciIzKorpe")
    public void izbaciIzKorpe(@RequestParam(required = true) String idKnjige,
                              HttpSession session, HttpServletResponse response) throws IOException {
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KORISNIK_KEY);
        System.out.println("Prijavljeni korisnik = " + prijavljeniKorisnik);
        Knjiga kniga = knjigaService.findOne(UUID.fromString(idKnjige));
        Korpa korpa = korpaService.findOneByKorisnikIdKorisnika(prijavljeniKorisnik.getIdKorisnika());
        // Proveravamo da li je korisnik uopste ulogovan i da li je korisnik kupac.
        // Ako jeste prikazujemo stranicu a ako nije vracamo na pocetnu.
        if(prijavljeniKorisnik == null || prijavljeniKorisnik.getUloga() != Uloga.KUPAC) {
            response.sendRedirect(baseURL + "/");
            return;
        } else {
            KorpaKnjiga korpaKnjiga = korpa.getStavke().stream().filter(korpaKnjiga1 -> korpaKnjiga1.getKnjiga().getIdKnjige().equals(kniga.getIdKnjige())).findFirst().get();
            boolean uspeh = korpa.getStavke().remove(korpaKnjiga);
            uspeh = uspeh && korpaService.update(korpa) > 0;
            if(uspeh) {
                kniga.setBrojPrimeraka(kniga.getBrojPrimeraka() + korpaKnjiga.getKolicina());
                knjigaService.update(kniga);
            }
        }
        response.sendRedirect(baseURL + "Korpa");
    }
}
