package com.ftn.ProjekatOWP.controllers;

import com.ftn.ProjekatOWP.dao.AutorDAO;
import com.ftn.ProjekatOWP.dao.KomentarDAO;
import com.ftn.ProjekatOWP.dao.LoyaltyKarticaDAO;
import com.ftn.ProjekatOWP.dao.ZanrDAO;
import com.ftn.ProjekatOWP.enums.Pismo;
import com.ftn.ProjekatOWP.enums.Povez;
import com.ftn.ProjekatOWP.enums.Status;
import com.ftn.ProjekatOWP.enums.Uloga;
import com.ftn.ProjekatOWP.models.*;
import com.ftn.ProjekatOWP.services.AutorService;
import com.ftn.ProjekatOWP.services.KnjigaService;
import com.ftn.ProjekatOWP.services.impl.KnjigaServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping(value = "/")
public class IndexController implements ServletContextAware {

    @Autowired
    private KnjigaService knjigaService;

    @Autowired
    private AutorService autorService;

    @Autowired
    private ZanrDAO zanrDAO;

    @Autowired
    private AutorDAO autorDAO;

    @Autowired
    private KomentarDAO komentarDAO;

    @Autowired
    private LoyaltyKarticaDAO loyaltyKarticaDAO;

    @Autowired
    private ServletContext servletContext;

    private String baseURL;

    public static final String KORISNIK_KEY = "prijavljeniKorisnik";


    @Override
    public void setServletContext(ServletContext servletContext) {
        baseURL = servletContext.getContextPath() + "/";
    }

    private static String PUTANJA_PRIKAZA_SLIKA = "/slike/";
    private static String PUTANJA_CUVANJA_SLIKA = "src/main/resources/static/slike/";


    @GetMapping
    public ModelAndView index(
            @RequestParam(required = false) String naziv,
            @RequestParam(required = false) String idZanra,
            @RequestParam(required = false) Double cenaOd,
            @RequestParam(required = false) Double cenaDo,
            @RequestParam(required = false) String ISBN,
            @RequestParam(required = false) String jezik,
            @RequestParam(required = false) String autor,
            @RequestParam(required = false) String sort,
            HttpSession session, HttpServletRequest request) {


        System.out.println("uuid " + UUID.randomUUID());

        String url = "";
        if(request.getQueryString() != null) {
            url = request.getRequestURL() + "?" + request.getQueryString();
        } else {
            url = request.getRequestURL().toString().substring(0, request.getRequestURL().toString().lastIndexOf("/"));
        }

        System.out.println("url " + url);

        if(autor != null) {
            if(autor.equals("")) {
                autor = null;
            }
        }

        if(idZanra != null) {
            if(idZanra.equals("")) {
                idZanra = null;
            }
        }

        List<Knjiga> knjige = knjigaService.find(naziv, ISBN, cenaOd, cenaDo, jezik, idZanra==null? null : zanrDAO.findOne(UUID.fromString(idZanra)), autor==null? null : autorDAO.findOneByImePrezime(autor.split(" ")[0], autor.split(" ")[1]), sort);


        List<Zanr> zanrovi = zanrDAO.findAll();
        List<Autor> autori = autorDAO.findAll();



        ModelAndView rezultat = new ModelAndView("index");

        String poruka = new String("Unesite vase korisnicko ime!");
        rezultat.addObject("poruka", poruka);
        rezultat.addObject("knjige", knjige);
        rezultat.addObject("zanrovi", zanrovi);
        rezultat.addObject("autori", autori);
        rezultat.addObject("url", url);

        return rezultat;
    }

    @GetMapping(value = "/Detalji")
    public ModelAndView detalji(@RequestParam String idKnjige, HttpSession session, HttpServletRequest request, HttpServletResponse response) {

        Knjiga knjiga = knjigaService.findOne(UUID.fromString(idKnjige));
        List<Zanr> zanrovi = zanrDAO.findAll();
        List<Autor> autori = autorDAO.findAll();

        ModelAndView rezultat = new ModelAndView("KnjigaDetalji");
        rezultat.addObject("knjiga", knjiga);
        // Sto li sam ja radio ovo da mi je znati?
        rezultat.addObject("zanrovi", zanrovi);
        rezultat.addObject("autori", autori);

        return rezultat;
    }

    @GetMapping(value = "/Kreiraj")
    public ModelAndView kreiraj(HttpSession session, HttpServletResponse response) throws IOException {
        
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
        if(prijavljeniKorisnik == null || prijavljeniKorisnik.getUloga() != Uloga.ADMINISTRATOR) {
            response.sendRedirect(baseURL + "/");
            return null;
        }

        List<Zanr> zanrovi = zanrDAO.findAll();

        ModelAndView rezultat = new ModelAndView("DodavanjeKnjige");
        rezultat.addObject("zanrovi", zanrovi);
        return rezultat;
    }


    @PostMapping(value = "/Kreiraj", consumes = {"multipart/form-data"})
    public void kreiraj(@RequestParam String naziv, @RequestParam String ISBN, @RequestParam String izdavackaKuca, @RequestParam int godinaIzdavanja,
                        @RequestParam String kratakOpis, @RequestParam double cena, @RequestParam int brojStranica, @RequestParam String povez,
                        @RequestParam String pismo, @RequestParam String jezik, @RequestParam MultipartFile slika, @RequestParam(name = "idZanra", required = false) String[] idZanrova,
                        @RequestParam(name = "autori", required = false) String autori, HttpSession session, HttpServletResponse response) throws Exception {

        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
        if(prijavljeniKorisnik == null || !prijavljeniKorisnik.getUloga().equals(Uloga.ADMINISTRATOR)) {
            response.sendRedirect(baseURL + "Index");
            return;
        }

        Path copyLocation = Paths.get(PUTANJA_CUVANJA_SLIKA + StringUtils.cleanPath(slika.getOriginalFilename()));
        System.out.println("Putanja cuvanja " + copyLocation);
        System.out.println("Putanja prikaza " + PUTANJA_PRIKAZA_SLIKA + StringUtils.capitalize(slika.getOriginalFilename()));
        Files.copy(slika.getInputStream(), copyLocation, StandardCopyOption.REPLACE_EXISTING);

        Knjiga knjiga = new Knjiga(UUID.randomUUID(), naziv, ISBN, izdavackaKuca, godinaIzdavanja, kratakOpis,
                PUTANJA_PRIKAZA_SLIKA + StringUtils.cleanPath(slika.getOriginalFilename()), cena, brojStranica, Povez.valueOf(povez), Pismo.valueOf(pismo), jezik, 0);

        System.out.println("Id zanrova " + idZanrova);

        for(String s:idZanrova) {
            knjiga.getZanrovi().add(zanrDAO.findOne(UUID.fromString(s)));
        }

        for(Autor a:autorService.parsirajAutoreIzStringa(autori)) {
            knjiga.getAutori().add(a);
        }

        knjigaService.save(knjiga);
        response.sendRedirect(baseURL + "/");
    }

    @GetMapping(value = "/Izmeni")
    public ModelAndView edit(@RequestParam String idKnjige, HttpSession session, HttpServletResponse response) throws IOException {
//        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
//        if(prijavljeniKorisnik == null || !prijavljeniKorisnik.getUloga().equals(Uloga.ADMINISTRATOR)) {
//            response.sendRedirect(baseURL + "/");
//            return;
//        }
        System.out.println("Get");
        Knjiga knjiga = knjigaService.findOne(UUID.fromString(idKnjige));

        if(knjiga == null) {
            response.sendRedirect(baseURL + "Knjizara");
            return null;
        } else {

        }

        ModelAndView rezultat = new ModelAndView("IzmenaKnjiga");
        rezultat.addObject("knjiga", knjiga);
        rezultat.addObject("autori", KnjigaServiceImpl.ispisiAutoreKnjige(knjiga));
        rezultat.addObject("zanrovi", zanrDAO.findAll());


        return rezultat;
    }

    @PostMapping(value = "/Izmeni", consumes = "multipart/form-data")
    public void edit(@RequestParam String id, @RequestParam String naziv, @RequestParam String ISBN, @RequestParam String izdavackaKuca,
                     @RequestParam int godinaIzdavanja, @RequestParam String kratakOpis, @RequestParam double cena, @RequestParam int brojStranica,
                     @RequestParam String povez, @RequestParam String pismo, @RequestParam String jezik, @RequestParam(required = false) MultipartFile slika,
                     @RequestParam(name = "idZanrova", required = false) String[] idZanrova,
                     @RequestParam(name = "idAutora", required = false) String idAutora, HttpSession session,
                     HttpServletResponse response) throws IOException {
        System.out.println("Post");

        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
        if(prijavljeniKorisnik == null || !prijavljeniKorisnik.getUloga().equals(Uloga.ADMINISTRATOR)) {
            response.sendRedirect(baseURL + "/");
            return;
        }

        System.out.println("idKnjige = " + id);
        Knjiga knjiga = knjigaService.findOne(UUID.fromString(id));
        if(knjiga == null) {

            response.sendRedirect(baseURL + "/");
            return;
        }


        if(naziv == null || naziv.equals("") || izdavackaKuca.equals("") || godinaIzdavanja < 100 || cena < 0
        || brojStranica <= 0 || jezik.equals("")) {
            response.sendRedirect(baseURL + "/Detalji?idKnjige=" + id);
            return;
        }



        System.out.println("Stara knjiga = " + knjiga);
        System.out.println("Broj autora = " + knjiga.getAutori().size());

        knjiga.setNaziv(naziv);
        knjiga.setIzdavackaKuca(izdavackaKuca);
        knjiga.setGodinaIzdavanja(godinaIzdavanja);
        knjiga.setKratakOpis(kratakOpis);
        //knjiga.setSlika(); Implementiraj!!!
        knjiga.setCena(cena);
        System.out.println("Povez " + povez);
        knjiga.setBrojStranica(brojStranica);
        knjiga.setPovez(Povez.valueOf(povez));
        knjiga.setPismo(Pismo.valueOf(pismo));
        knjiga.setJezik(jezik);
        if(!slika.isEmpty()) {
            Path copyLocation = Paths.get(PUTANJA_CUVANJA_SLIKA + StringUtils.cleanPath(slika.getOriginalFilename()));
            Files.copy(slika.getInputStream(), copyLocation, StandardCopyOption.REPLACE_EXISTING);
            knjiga.setSlika(PUTANJA_PRIKAZA_SLIKA + StringUtils.cleanPath(slika.getOriginalFilename()));
        }

        System.out.println("idZanrova " + idZanrova);

        knjiga.getZanrovi().clear();
        for(String s:idZanrova) {
            knjiga.getZanrovi().add(zanrDAO.findOne(UUID.fromString(s)));
        }

        try {
            knjiga.getAutori().clear();
            for(Autor a:autorService.parsirajAutoreIzStringa(idAutora)) {
                knjiga.getAutori().add(a);
            }
        } catch (Exception ex) {
            System.out.println("Exceprionnn ");
            ex.printStackTrace();
            response.sendRedirect(baseURL + "Izmeni?idKnjige=" + id);
            return;
        }

        System.out.println("Nova knjiga  = " + knjiga);
        System.out.println("Broj autora = " + knjiga.getAutori().size());
        knjigaService.update(knjiga);

        response.sendRedirect(baseURL);
    }


//    @PostMapping(value = "/Dodavanje")
//    public void dodavanje(@RequestParam String id, @RequestParam String naziv, @RequestParam String ISBN, @RequestParam String izdavackaKuca,
//                     @RequestParam int godinaIzdavanja, @RequestParam String kratakOpis, @RequestParam double cena, @RequestParam int brojStranica,
//                     @RequestParam String povez, @RequestParam String pismo, @RequestParam String jezik,
//                     @RequestParam(name = "idZanrova", required = false) String[] idZanrova,
//                     @RequestParam(name = "idAutora", required = false) String idAutora, HttpSession session,
//                     HttpServletResponse response) throws IOException {
//        System.out.println("Post");
//
////        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
////        if(prijavljeniKorisnik == null || !prijavljeniKorisnik.getUloga().equals(Uloga.ADMINISTRATOR)) {
////            response.sendRedirect(baseURL + "/");
////            return;
////        }
//
//
//
//        if(naziv == null || naziv.equals("") ||  ISBN.length() < 17  || izdavackaKuca.equals("") || godinaIzdavanja < 100 || cena < 0
//                || brojStranica <= 0 || jezik.equals("")) {
//            response.sendRedirect(baseURL + "/Detalji?idKnjige=" + id);
//            return;
//        }
//
//        Knjiga knjiga = new Knjiga(id, naziv, ISBN, izdavackaKuca, godinaIzdavanja, kratakOpis, );
//
//        System.out.println("Stara knjiga = " + knjiga);
//        System.out.println("Broj autora = " + knjiga.getAutori().size());
//
//        knjiga.setNaziv(naziv);
//        knjiga.setISBN(ISBN);
//        knjiga.setIzdavackaKuca(izdavackaKuca);
//        knjiga.setGodinaIzdavanja(godinaIzdavanja);
//        knjiga.setKratakOpis(kratakOpis);
//        //knjiga.setSlika(); Implementiraj!!!
//        knjiga.setCena(cena);
//        System.out.println("Povez " + povez);
//        knjiga.setBrojStranica(brojStranica);
//        knjiga.setPovez(Povez.valueOf(povez));
//        knjiga.setPismo(Pismo.valueOf(pismo));
//        knjiga.setJezik(jezik);
//
//        System.out.println("idZanrova " + idZanrova);
//
//        knjiga.getZanrovi().clear();
//        for(String s:idZanrova) {
//            knjiga.getZanrovi().add(zanrDAO.findOne(UUID.fromString(s)));
//        }
//
//        try {
//            knjiga.getAutori().clear();
//            for(Autor a:autorService.parsirajAutoreIzStringa(idAutora)) {
//                knjiga.getAutori().add(a);
//            }
//        } catch (Exception ex) {
//            System.out.println("Exceprionnn");
//            response.sendRedirect(baseURL + "Izmeni?idKnjige=" + id);
//            return;
//        }
//
//        System.out.println("Nova knjiga  = " + knjiga);
//        System.out.println("Broj autora = " + knjiga.getAutori().size());
//        knjigaService.update(knjiga);
//
//        response.sendRedirect(baseURL);
//    }


    @PostMapping(value = "/Obrisi")
    public void delete(@RequestParam String idKnjige, HttpSession session, HttpServletResponse response) throws IOException {
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
        if(prijavljeniKorisnik == null || !prijavljeniKorisnik.getUloga().equals(Uloga.ADMINISTRATOR)) {
            response.sendRedirect(baseURL + "/");
            return;
        }

        knjigaService.delete(UUID.fromString(idKnjige));

        response.sendRedirect(baseURL + "/");
    }

    @GetMapping(value = "/DodajKomentar")
    public ModelAndView prikazFormeZaUnosKomentara(@RequestParam String idKnjige, HttpSession session, HttpServletResponse response, HttpServletRequest request) throws IOException {

        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
        //Vraca na pocetnu stranicu ako je korisnik admin ili je neulogovan.
        if(prijavljeniKorisnik == null || prijavljeniKorisnik.getUloga().equals(Uloga.ADMINISTRATOR)) {
            ModelAndView rezultat = new ModelAndView("index");

            List<Knjiga> knjige = knjigaService.findAll();
            List<Zanr> zanrovi = zanrDAO.findAll();
            List<Autor> autori = autorDAO.findAll();

            String url = "";
            if(request.getQueryString() != null) {
                url = request.getRequestURL() + "?" + request.getQueryString();
            } else {
                url = request.getRequestURL().toString().substring(0, request.getRequestURL().toString().lastIndexOf("/"));
            }

            rezultat.addObject("knjige", knjige);
            rezultat.addObject("zanrovi", zanrovi);
            rezultat.addObject("autori", autori);
            rezultat.addObject("url", url);

            return rezultat;
        }

        //Odlazimo na stranicu za dodavanje komentara ako je korisnik ulogovan i nije admin.
        ModelAndView rezultat = new ModelAndView("DodavanjeKomentara");
        rezultat.addObject("knjiga", knjigaService.findOne(UUID.fromString(idKnjige)));
        //Mislim da ovo nije ni potrebno jer mogu da ga izvadim iz sesije.
        //rezultat.addObject("korisnik", prijavljeniKorisnik);
        return rezultat;
    }

    //do ovde smo stigli  jbg
    @PostMapping(value = "/DodajKomentar")
    public void obradaUnosaKomentara(@RequestParam String idKnjige, @RequestParam String tekstKomentara,
                                             @RequestParam int ocena, HttpSession session, HttpServletResponse response) throws IOException {

        System.out.println("Ostavljen komentar: idKnjige: " +idKnjige+ " ocena knjige: " + ocena + " tekst komentara: " +tekstKomentara);

        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);

        if(prijavljeniKorisnik == null || !prijavljeniKorisnik.getUloga().equals(Uloga.KUPAC)) {
            response.sendRedirect(baseURL + "/");
            return;
        }

        Knjiga knjiga = knjigaService.findOne(UUID.fromString(idKnjige));
        Komentar komentar = komentarDAO.findByKnjigaAutor(knjiga.getIdKnjige(), prijavljeniKorisnik.getIdKorisnika());
        if(komentar == null) {
            komentar = new Komentar(UUID.randomUUID(), tekstKomentara, ocena, LocalDate.now(), prijavljeniKorisnik, knjiga, Status.NA_CEKANJU);
            komentarDAO.save(komentar);
        } else {
            komentar.setTekstKomentara(tekstKomentara);
            komentar.setOcena(ocena);
            komentar.setDatumKomentara(LocalDate.now());
            komentar.setStatus(Status.NA_CEKANJU);
            komentarDAO.update(komentar);
        }

        response.sendRedirect(baseURL + "Detalji?idKnjige=" + idKnjige);
        return;
    }

    @GetMapping("/Komentari")
    @ResponseBody
    public Map<String, Object> vratiKomentare(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("Pogodjen endpoint za pribavljanje komentra");
        Map<String, Object> odgovor = new LinkedHashMap<>();
        List<Komentar> komentari = komentarDAO.findAll();

        odgovor.put("odgovor", "ok");
        odgovor.put("komentari", komentari);

        return odgovor;
    }
}
