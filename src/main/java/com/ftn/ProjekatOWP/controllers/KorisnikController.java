package com.ftn.ProjekatOWP.controllers;

import com.ftn.ProjekatOWP.dao.AutorDAO;
import com.ftn.ProjekatOWP.dao.KorisnikDAO;
import com.ftn.ProjekatOWP.dao.LoyaltyKarticaDAO;
import com.ftn.ProjekatOWP.dao.ZanrDAO;
import com.ftn.ProjekatOWP.enums.Status;
import com.ftn.ProjekatOWP.enums.Uloga;
import com.ftn.ProjekatOWP.models.*;
import com.ftn.ProjekatOWP.services.KnjigaService;
import com.ftn.ProjekatOWP.services.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping(value = "/Korisnici")
public class KorisnikController {

    public static final String KORISNIK_KEY = "prijavljeniKorisnik";

    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private KnjigaService knjigaService;

    @Autowired
    private AutorDAO autorDAO;

    @Autowired
    private ZanrDAO zanrDAO;

    @Autowired
    private LoyaltyKarticaDAO loyaltyKarticaDAO;

    @Autowired
    private ServletContext servletContext;
    private String baseURL;

    @PostConstruct
    public void init() {
        baseURL = servletContext.getContextPath() + "/";
    }

    @PostMapping(value = "/Prijavljivanje")
    public void postLogin(@RequestParam String korisnickoIme, @RequestParam String lozinka, HttpSession session,
                                  HttpServletResponse response) throws IOException {

        System.out.println("Username " + korisnickoIme + " Lozinka " + lozinka);
        try {
            Korisnik korisnik = korisnikService.findOne(korisnickoIme, lozinka);

            if(korisnik == null) {
                throw new Exception("Neispravno korisnicko ime ili lozinka");
            }

            session.setAttribute(KorisnikController.KORISNIK_KEY, korisnik);
            System.out.println("Base urlllllasddas jebano");
            response.sendRedirect(baseURL);
//            return null;
        } catch (Exception ex) {
            String poruka = ex.getMessage();
            if(poruka == "") {
                poruka = "Neuspesna prijava!";
            }

            System.out.println("Ovo se izvrsava");
            ModelAndView rezultat = new ModelAndView("index");
            List<Knjiga> knjige = knjigaService.findAll();

            List<Zanr> zanrovi = zanrDAO.findAll();
            List<Autor> autori = autorDAO.findAll();
            rezultat.addObject("poruka", poruka);
            rezultat.addObject("knjige", knjige);
            rezultat.addObject("zanrovi", zanrovi);
            rezultat.addObject("autori", autori);

//            return rezultat;
            response.sendRedirect(baseURL);
        }
    }

    @GetMapping(value = "/Registracija")
    public ModelAndView registracija(HttpSession session, HttpServletResponse response) throws IOException {

        ModelAndView rezultat = new ModelAndView("Registracija");
        korisnikService.ocistiSesiju(session);

        return rezultat;
    }

    @PostMapping(value = "/Registracija")
    public ModelAndView registracija(HttpSession session, HttpServletResponse response, @RequestParam String korisnicko, @RequestParam String password, @RequestParam String repeatedPassword, @RequestParam String email,
                             @RequestParam String ime, @RequestParam String prezime, @RequestParam(required = false) @DateTimeFormat (iso=DateTimeFormat.ISO.DATE) LocalDate datumRodjenja, @RequestParam String adresa, @RequestParam String brojTelefona) throws IOException {

        //Proveravamo ispravnost podataka
        String greska = korisnikService.proveriIspravnostPodataka(korisnicko, password, repeatedPassword, email, ime, prezime, datumRodjenja, adresa, brojTelefona);
        if(greska.equals("")) {

            Korisnik noviKorisnik = new Korisnik(UUID.randomUUID(), korisnicko, password, email, ime, prezime, datumRodjenja,
                    adresa, brojTelefona, LocalDateTime.now(), Uloga.KUPAC, false);
            korisnikService.save(noviKorisnik);
            korisnikService.ocistiSesiju(session);
            response.sendRedirect(baseURL);
            return null;
        } else {
            // U slucaju da korisnik sjebe registraciju cuvaju se podaci da kod
            // ponovnog pokusaja podaci budu ponovo popunjeni.
            ModelAndView rezultat = new ModelAndView("registracija");
            rezultat.addObject("poruka", greska);
            session.setAttribute("korisnicko", korisnicko);
            session.setAttribute("password", password);
            session.setAttribute("repeatedPassword", repeatedPassword);
            session.setAttribute("email", email);
            session.setAttribute("ime", ime);
            session.setAttribute("prezime", prezime);
            session.setAttribute("datumRodjenja", datumRodjenja);
            session.setAttribute("adresa", adresa);
            session.setAttribute("brojTelefona", brojTelefona);

            return rezultat;

        }
    }

    @GetMapping("/LoyaltyKartica")
    public ModelAndView loyaltiKartica(HttpSession session, HttpServletResponse response) throws IOException {

        Korisnik ulogovanKorisnik = (Korisnik) session.getAttribute(KORISNIK_KEY);

        ModelAndView rezultat = new ModelAndView("LoyaltyKartica");

        if(ulogovanKorisnik != null && ulogovanKorisnik.getUloga().equals(Uloga.KUPAC)) {
            LoyaltyKartica loyaltyKartica = loyaltyKarticaDAO.findByKorisnik(ulogovanKorisnik.getIdKorisnika());
            rezultat.addObject("kartica", loyaltyKartica);
        } else {
            response.sendRedirect(baseURL);
            return null;
        }

        return rezultat;
    }

    @GetMapping("/ZatraziKarticu")
    public void zatraziKarticu(HttpSession session, HttpServletResponse response) throws IOException {
        Korisnik ulogovanKorisnik = (Korisnik) session.getAttribute(KORISNIK_KEY);
        if (ulogovanKorisnik == null && !ulogovanKorisnik.getUloga().equals(Uloga.KUPAC)) {
            response.sendRedirect(baseURL);
            return;
        }

        LoyaltyKartica loyaltyKartica = loyaltyKarticaDAO.findByKorisnik(ulogovanKorisnik.getIdKorisnika());

        if(loyaltyKartica == null) {
            loyaltyKartica = new LoyaltyKartica(UUID.randomUUID(),
                    ulogovanKorisnik, 4, Status.NA_CEKANJU);
            loyaltyKarticaDAO.save(loyaltyKartica);
        } else if(loyaltyKartica.getStatusKartice().equals(Status.NIJE_ODOBREN)) {
            loyaltyKartica.setStatusKartice(Status.NA_CEKANJU);
            loyaltyKarticaDAO.update(loyaltyKartica);
        }

        response.sendRedirect(baseURL + "Korisnici/LoyaltyKartica");
        return;
    }

    @GetMapping(value="/Odjava")
    public void odjava(HttpSession session, HttpServletResponse response) throws IOException {
        // odjava
        session.invalidate();

        response.sendRedirect(baseURL);
    }

    @GetMapping
    public ModelAndView korisnici(HttpSession session, HttpServletRequest request, HttpServletResponse response) {

        List<Korisnik> korisnici = korisnikService.findAll();
        System.out.println("Svi korisnici");
        System.out.println(korisnici);
        ModelAndView rezultat = new ModelAndView("PrikazSvihKorisnika");
        rezultat.addObject("korisnici", korisnici);
        return rezultat;
    }

    //Nezavrseno
    @GetMapping(value = "/Detalji")
    public ModelAndView korisnik(@RequestParam String idKorisnika, HttpSession session, HttpServletRequest request, HttpServletResponse response) {

        Korisnik korisnik = korisnikService.findOne(UUID.fromString(idKorisnika));
        System.out.println("Nadjen korisnik " + korisnik);

        ModelAndView rezultat =  new ModelAndView("KorisnikDetalji");
        rezultat.addObject("korisnik", korisnik);

        return rezultat;
    }

//    @GetMapping(value = "/Izmeni")
//    public ModelAndView izmeniKorisnika(@RequestParam String idKorisnika, HttpSession session, HttpServletResponse response) throws IOException {
//        Korisnik korisnik = korisnikService.findOne(UUID.fromString(idKorisnika));
//
//        if(korisnik == null) {
//            response.sendRedirect(baseURL + "Knjzara");
//            return null;
//        } else {
//
//        }
//
//        ModelAndView rezultat = new ModelAndView("IzmenaKorisnika");
//        rezultat.addObject("korisnik", korisnik);
//
//
//        return rezultat;
//    }

    @PostMapping(value = "/Izmeni")
    public void izmeniKorisnika(@RequestParam String idKorisnika, @RequestParam String ulogaKorisnika,
                                @RequestParam(required = false) String[] blokiran, HttpSession session,
                                HttpServletResponse response) throws IOException {



        Korisnik korisnik = korisnikService.findOne(UUID.fromString(idKorisnika));

        if(korisnik == null) {
            response.sendRedirect(baseURL + "/");
        }
        System.out.println("Stari korisnik " + korisnik);

        korisnik.setBlokiran((blokiran == null)? false:true);
        korisnik.setUloga(Uloga.valueOf(ulogaKorisnika));
        korisnikService.update(korisnik);
        System.out.println("Novi korisnik " +korisnik);

        response.sendRedirect(baseURL + "Korisnici");
    }


}
