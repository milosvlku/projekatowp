package com.ftn.ProjekatOWP.controllers;

import com.ftn.ProjekatOWP.enums.Uloga;
import com.ftn.ProjekatOWP.models.Knjiga;
import com.ftn.ProjekatOWP.models.Korisnik;
import com.ftn.ProjekatOWP.models.ListaZelja;
import com.ftn.ProjekatOWP.services.KnjigaService;
import com.ftn.ProjekatOWP.services.ListaZeljaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.plaf.PanelUI;
import java.io.IOException;
import java.util.Set;
import java.util.UUID;
import java.util.function.Predicate;

@Controller
@RequestMapping(value = "ListaZelja")
public class ListaZeljaController {

    public static final String KORISNIK_KEY = "prijavljeniKorisnik";

    @Autowired
    private ServletContext servletContext;

    private String baseURL;

    @Autowired
    private ListaZeljaService listaZeljaService;

    @Autowired
    private KnjigaService knjigaService;

    @PostConstruct
    public void init() {
        baseURL = servletContext.getContextPath() + "/";
    }

    @GetMapping
    public ModelAndView listaZelja(HttpSession session, HttpServletResponse response) throws IOException {
        System.out.println("Test");
        Korisnik prijavljeniKorisnik =  (Korisnik) session.getAttribute(KORISNIK_KEY);
        if(prijavljeniKorisnik == null || prijavljeniKorisnik.getUloga() != Uloga.KUPAC) {
            response.sendRedirect(baseURL + "/");
            return null;
        }

        ListaZelja listaZelja = listaZeljaService.findOneByKorisnikIdKorisnika(prijavljeniKorisnik.getIdKorisnika());
        ModelAndView rezultat = new ModelAndView("ListaZelja");
        rezultat.addObject("korisnik", prijavljeniKorisnik);
        rezultat.addObject("listaZelja", listaZelja);
        listaZelja.getKnjige().stream().forEach(knjiga -> System.out.println("Knjiga " + knjiga.getZanrovi()));
        return rezultat;
    }


    @PostMapping(value = "/DodajUkloniIzListeZelja")
    public void dodajUkloniIzListeZelja(@RequestParam(required = true) String idKnjige,
                                                HttpSession session, HttpServletResponse response) throws IOException {
        System.out.println("Dodaj u listu zelja kontroller!");
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(
                KORISNIK_KEY
        );


        Knjiga knjiga = knjigaService.findOne(UUID.fromString(idKnjige));
        if(prijavljeniKorisnik == null || prijavljeniKorisnik.getUloga() != Uloga.KUPAC) {
            response.sendRedirect(baseURL + "/");
        } else {
            ListaZelja listaZelja = listaZeljaService.findOneByKorisnikIdKorisnika(
                    prijavljeniKorisnik.getIdKorisnika()
            );
            if(listaZelja == null) {
                System.out.println("Korisnik nije imao listu zelkja");
                listaZelja = new ListaZelja(UUID.randomUUID(), prijavljeniKorisnik);
                listaZelja.getKnjige().add(knjiga);
                listaZeljaService.save(listaZelja);

            } else {
                boolean knjigaJeUListiZelja = listaZelja.getKnjige().stream().filter(knjiga1 -> knjiga1.getIdKnjige().toString().equals(knjiga.getIdKnjige().toString())).findFirst().isPresent();
                if(knjigaJeUListiZelja) {
                    System.out.println("Knjiga se vec nalazi u listi zelja, vadimo napolje.");
                    listaZelja.getKnjige().remove(listaZelja.getKnjige().stream().filter(knjiga1 -> knjiga1.getIdKnjige().toString().equals(knjiga.getIdKnjige().toString())).findFirst().get());

                    listaZeljaService.update(listaZelja);
                } else {
                    System.out.println("Knjiga nije postojala u listi zelja, dodajemo!");
                    listaZelja.getKnjige().add(knjiga);
                    listaZeljaService.update(listaZelja);
                }
            }

            response.sendRedirect(baseURL + "/Detalji?idKnjige=" + idKnjige);
        }
    }
}
