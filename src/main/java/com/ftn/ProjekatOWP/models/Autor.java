package com.ftn.ProjekatOWP.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class Autor {
    private UUID idAutora;
    private String imeAutora;
    private String prezimeAutora;
}
