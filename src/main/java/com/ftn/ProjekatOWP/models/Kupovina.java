package com.ftn.ProjekatOWP.models;

import lombok.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@ToString
public class Kupovina {
    @NonNull private UUID idKupovine;
    private List<KupljenaKnjiga> kupovinaOdredjeneKnjige = new ArrayList<>();
    private double ukupnaCenaKupovine;
    @NonNull private LocalDate datumKupovine;
    @NonNull private Korisnik korisnik;
    @NonNull private int brojUtrosenihPoena;

}
