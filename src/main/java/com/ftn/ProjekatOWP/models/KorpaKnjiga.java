package com.ftn.ProjekatOWP.models;

import lombok.*;

import java.util.UUID;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
public class KorpaKnjiga {
    @NonNull private UUID idKorpeKnjige;
    @NonNull private Korpa korpa;
    @NonNull private Knjiga knjiga;
    private int kolicina;
}
