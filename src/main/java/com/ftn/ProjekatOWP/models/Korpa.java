package com.ftn.ProjekatOWP.models;

import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
public class Korpa {
    @NonNull private UUID idKorpe;
    @NonNull private Korisnik korisnik;
    List<KorpaKnjiga> stavke = new ArrayList<>();
}
