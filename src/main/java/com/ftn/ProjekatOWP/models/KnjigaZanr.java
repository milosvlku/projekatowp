package com.ftn.ProjekatOWP.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class KnjigaZanr {
    private UUID idKnjigeZanr;
    private Knjiga knjiga;
    private Zanr zanr;
}
