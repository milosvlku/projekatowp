package com.ftn.ProjekatOWP.models;

import lombok.Data;

import java.time.LocalDate;
import java.util.UUID;

@Data
public class SpecijalniDatum {

    private UUID idDatuma;
    private LocalDate datum;
    private int popust;

}
