package com.ftn.ProjekatOWP.models;

import lombok.*;
import org.springframework.stereotype.Repository;

import java.util.*;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
public class ListaZelja {
    @NonNull private UUID idListeZelja;
    @NonNull private Korisnik korisnik;
    private Set<Knjiga> knjige = new HashSet<>();
}
