package com.ftn.ProjekatOWP.models;

import com.ftn.ProjekatOWP.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Data
@RequiredArgsConstructor
public class LoyaltyKartica {

    @NonNull private UUID idLoyaltyKartice;
    @NonNull private Korisnik korisnik;
    @NonNull private int brojPoena;
    @NonNull private Status statusKartice;

}
