package com.ftn.ProjekatOWP.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class KnjigaAutor {
    private UUID idKnjigeAutora;
    private Knjiga knjiga;
    private Autor autor;
}
