package com.ftn.ProjekatOWP.models;

import lombok.*;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListaZeljaKnjiga {
    @NonNull private UUID idListaZeljaKnjiga;
    @NonNull private ListaZelja listaZelja;
    @NonNull private Knjiga knjiga;
}
