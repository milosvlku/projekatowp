package com.ftn.ProjekatOWP.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.UUID;

@Data
@AllArgsConstructor
@ToString
public class KupljenaKnjiga {

    private UUID idKupovineKnjige;
    private Kupovina kupovina;
    private Knjiga knjiga;
    private double brojPrimeraka;
    /*private Double cenaKupljenihKnjiga;*/

}
