package com.ftn.ProjekatOWP.models;

import com.ftn.ProjekatOWP.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.util.UUID;

@Data
@AllArgsConstructor
public class Komentar {
    private UUID idKomentara;
    private String tekstKomentara;
    private int ocena;
    private LocalDate datumKomentara;
    private Korisnik autorKomentara;
    private Knjiga knjiga;
    private Status status;
}
