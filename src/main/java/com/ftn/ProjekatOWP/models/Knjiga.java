package com.ftn.ProjekatOWP.models;

import com.ftn.ProjekatOWP.enums.Pismo;
import com.ftn.ProjekatOWP.enums.Povez;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
public class Knjiga {
    @NonNull private UUID idKnjige;
    @NonNull private String naziv;
    @NonNull private String ISBN;
    @NonNull private String izdavackaKuca;
    private List<Autor> autori = new ArrayList<>();
    private List<Zanr> zanrovi = new ArrayList<>();
    @NonNull private int godinaIzdavanja;
    @NonNull private String kratakOpis;
    @NonNull private String slika;
    @NonNull private double cena;
    @NonNull private int brojStranica;
    @NonNull private Povez povez;
    @NonNull private Pismo pismo;
    @NonNull private String jezik;
    @NonNull private int brojPrimeraka;
    private int ocena;
}
