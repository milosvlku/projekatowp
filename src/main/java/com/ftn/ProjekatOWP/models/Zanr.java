package com.ftn.ProjekatOWP.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class Zanr {

    private UUID idZanra;
    private String naziv;
    private String opis;

}
