package com.ftn.ProjekatOWP.models;

import com.ftn.ProjekatOWP.enums.Uloga;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
public class Korisnik {

    private UUID idKorisnika;
    private String korisnickoIme;
    private String lozinka;
    private String email;
    private String ime;
    private String prezime;
    private LocalDate datumRodjenja;
    private String adresa;
    private String brojTelefona;
    private LocalDateTime datumIVremeRegistracije;
    private Uloga uloga;
    private boolean blokiran;

}
