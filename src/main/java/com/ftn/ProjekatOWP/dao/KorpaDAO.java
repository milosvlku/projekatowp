package com.ftn.ProjekatOWP.dao;

import com.ftn.ProjekatOWP.models.Korpa;

import java.util.UUID;

public interface KorpaDAO {

    public Korpa findOne(UUID idKorpe);

    public Korpa findKorpaByKorisnikIdKorisnika(UUID idKorisnika);

    public int save(Korpa korpa);

    public int update(Korpa korpa);

    public int delete(UUID idKorpe);

}
