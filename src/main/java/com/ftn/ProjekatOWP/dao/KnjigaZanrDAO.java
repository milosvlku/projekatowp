package com.ftn.ProjekatOWP.dao;

import com.ftn.ProjekatOWP.models.KnjigaZanr;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public interface KnjigaZanrDAO {
    public KnjigaZanr findOne(UUID idKnjigaZanr);

    public List<KnjigaZanr> findAll();

    public List<KnjigaZanr> findByKnjiga(UUID idKnjige);

    public List<KnjigaZanr> findByZanr(UUID idZanra);

    public KnjigaZanr findByKnjigaZanr(UUID idKnjige, UUID idZanra);

    public int save(KnjigaZanr knjigaZanr);

//    public int [] save(ArrayList<KnjigaZanr> zanrovi);

    public int update(KnjigaZanr knjigaZanr);

    public int delete(UUID idKnjigaZanr);
}
