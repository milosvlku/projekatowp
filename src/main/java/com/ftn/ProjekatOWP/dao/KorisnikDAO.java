package com.ftn.ProjekatOWP.dao;

import com.ftn.ProjekatOWP.enums.Uloga;
import com.ftn.ProjekatOWP.models.Korisnik;

import java.util.List;
import java.util.UUID;

public interface KorisnikDAO {

    public Korisnik findOne(UUID idKorisnika);

    public Korisnik findOne(String username);

    public Korisnik findOne(String korisnickoIme, String lozinka);

    public List<Korisnik> findAll();

    public List<Korisnik> find(String korisnickoIme, Uloga uloga);

    public int save(Korisnik korisnik);

    public int update(Korisnik korisnik);

    public int delete(UUID idKorisnika);

}
