package com.ftn.ProjekatOWP.dao;

import com.ftn.ProjekatOWP.enums.Status;
import com.ftn.ProjekatOWP.models.Korisnik;
import com.ftn.ProjekatOWP.models.LoyaltyKartica;

import java.util.List;
import java.util.UUID;

public interface LoyaltyKarticaDAO {

    public LoyaltyKartica findOne(UUID idLoyaltyKartice);

    public LoyaltyKartica findByKorisnik(UUID idKorisnika);

    public List<LoyaltyKartica> findAll();

    public List<LoyaltyKartica> findByStatus(Status status);

    public int save(LoyaltyKartica loyaltyKartica);

    public int update(LoyaltyKartica loyaltyKartica);

    public int delete(LoyaltyKartica loyaltyKartica);

}
