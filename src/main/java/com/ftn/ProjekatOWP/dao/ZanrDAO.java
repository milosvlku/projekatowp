package com.ftn.ProjekatOWP.dao;

import com.ftn.ProjekatOWP.models.Zanr;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public interface ZanrDAO {

    public Zanr findOne(UUID idZanra);

    public List<Zanr> findAll();

    public List<Zanr> find(String naziv);

    public int save(Zanr zanr);

    public int [] save(ArrayList<Zanr> zanrovi);

    public int update(Zanr zanr);

    public int delete(UUID idZanra);
}
