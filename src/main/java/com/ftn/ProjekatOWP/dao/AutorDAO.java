package com.ftn.ProjekatOWP.dao;

import com.ftn.ProjekatOWP.models.Autor;
import com.ftn.ProjekatOWP.models.Zanr;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public interface AutorDAO {

    public Autor findOne(UUID idAutora);

    public Autor findOneByImePrezime(String imeAutora, String prezimeAutora);

    public List<Autor> findAll();


    public int save(Autor autor);

    /*public int [] save(ArrayList<Autor> autori);*/

    public int update(Autor autor);

    public int delete(UUID idAutora);
}
