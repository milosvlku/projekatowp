package com.ftn.ProjekatOWP.dao;

import com.ftn.ProjekatOWP.models.ListaZelja;
import com.ftn.ProjekatOWP.models.ListaZeljaKnjiga;

import java.util.Set;
import java.util.UUID;

public interface ListaZeljaKnjigaDAO {

    public ListaZeljaKnjiga findOne(UUID idListeZeljaKnjiga);

    public Set<ListaZeljaKnjiga> findByListaZeljaIdListeZelja(UUID idListaZelja);

    public ListaZeljaKnjiga findByListaZeljaIdListeZeljaAndKnjigaIdKnjige(UUID idListaZelja, UUID idKnjige);

    public int save(ListaZeljaKnjiga listaZeljaKnjiga);

    public int update(ListaZeljaKnjiga listaZeljaKnjiga);

    public int delete(UUID idListaZeljaKnjiga);

}
