package com.ftn.ProjekatOWP.dao;

import com.ftn.ProjekatOWP.models.Komentar;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public interface KomentarDAO {
    public Komentar findOne(UUID idKomentara);

    public List<Komentar> findAll();

    public List<Komentar> findByKnjiga(UUID idKnjige);

    public List<Komentar> findByAutor(UUID idAutora);

    public Komentar findByKnjigaAutor(UUID idKnjige, UUID idAutora);

    public int save(Komentar komenatar);

//    public int [] save(ArrayList<Komentar> zanrovi);

    public int update(Komentar komentar);

    public int delete(UUID idKomentara);
}
