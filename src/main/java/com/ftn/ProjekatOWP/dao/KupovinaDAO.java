package com.ftn.ProjekatOWP.dao;

import com.ftn.ProjekatOWP.models.Kupovina;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public interface KupovinaDAO {

    public Kupovina findOne(UUID idKupovine);

    public List<Kupovina> findAll();

    public List<Kupovina> findByKorisnikIdKorisnika(UUID idKorisnika);

    public int save(Kupovina kupovina);

//    public int [] save(ArrayList<Kupovina> kupovine);

    public int update(Kupovina kupovina);

    public int delete(UUID idKupovine);
}
