package com.ftn.ProjekatOWP.dao;

import com.ftn.ProjekatOWP.models.KorpaKnjiga;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface KorpaKnjigaDAO {

    public KorpaKnjiga findOne(UUID idKorpeKnjige);

    public List<KorpaKnjiga> findByKorpaIdKorpe(UUID idKorpe);

    public List<KorpaKnjiga> findByKnjigaIdKnjige(UUID idKnjige);

    public int save(KorpaKnjiga korpaKnjiga);

    public int update(KorpaKnjiga korpaKnjiga);

    public int delete(UUID idKorpeKnjige);

    public KorpaKnjiga findByKorpaIdKorpeAndKnjigaIdKnjige(UUID idKorpe, UUID idKnjige);
}
