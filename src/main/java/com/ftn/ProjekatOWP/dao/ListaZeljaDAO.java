package com.ftn.ProjekatOWP.dao;

import com.ftn.ProjekatOWP.models.ListaZelja;

import java.util.UUID;

public interface ListaZeljaDAO {

    public ListaZelja findOne(UUID idListeZelja);

    public ListaZelja findListaZeljaByKorisnikIdKorisnika(UUID idKorisnika);

    public int save(ListaZelja listaZelja);

    public int update(ListaZelja listaZelja);

    public int delete(UUID idListeZelja);


}
