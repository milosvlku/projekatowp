package com.ftn.ProjekatOWP.dao;

import com.ftn.ProjekatOWP.models.KnjigaAutor;
import com.ftn.ProjekatOWP.models.KnjigaZanr;

import java.util.List;
import java.util.UUID;

public interface KnjigaAutorDAO {

    public KnjigaAutor findOne(UUID idKnjigeAutora);

    public List<KnjigaAutor> findAll();

    public List<KnjigaAutor> findByKnjiga(UUID idKnjige);

    public List<KnjigaAutor> findByAutor(UUID idAutora);

    public KnjigaAutor findByKnjigaAutor(UUID idKnjige, UUID idAutora);

    public int save(KnjigaAutor knjigaAutor);

    public int update(KnjigaAutor knjigaAutor);

    public int delete(UUID idKnjigeAutora);

}
