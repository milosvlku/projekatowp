package com.ftn.ProjekatOWP.dao;

import com.ftn.ProjekatOWP.models.Knjiga;

import java.util.List;
import java.util.UUID;

public interface KnjigaDAO {

    public Knjiga findOne(UUID idKnjige);

    public List<Knjiga> findAll();

    public int save(Knjiga knjiga);

    public int update(Knjiga knjiga);

    public int delete(UUID idKnjige);

    public List<Knjiga> find(String naziv, String ISBN, Double cenaOd, Double cenaDo, String jezik);

}
