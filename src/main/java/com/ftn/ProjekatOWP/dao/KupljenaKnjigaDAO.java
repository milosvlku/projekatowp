package com.ftn.ProjekatOWP.dao;

import com.ftn.ProjekatOWP.models.KupljenaKnjiga;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public interface KupljenaKnjigaDAO {

    public KupljenaKnjiga findOne(UUID idKupovineKnjige);

    public List<KupljenaKnjiga> findAll();

    public List<KupljenaKnjiga> findAllByKupovinaIdKupovine(UUID idKupovine);

    /*public List<KupljenaKnjiga> find(String naziv);*/

    public int save(KupljenaKnjiga kupljenaKnjiga);

    /*public int [] save(ArrayList<KupljenaKnjiga> zanrovi);*/

    public int update(KupljenaKnjiga kupljenaKnjiga);

    public int delete(UUID idKupovineKnjige);
}
