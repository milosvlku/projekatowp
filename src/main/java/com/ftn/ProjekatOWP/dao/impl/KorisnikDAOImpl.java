package com.ftn.ProjekatOWP.dao.impl;

import com.ftn.ProjekatOWP.dao.KorisnikDAO;
import com.ftn.ProjekatOWP.enums.Uloga;
import com.ftn.ProjekatOWP.models.Korisnik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
public class KorisnikDAOImpl implements KorisnikDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class KorisnikRowMapper implements RowMapper<Korisnik> {

        @Override
        public Korisnik mapRow(ResultSet resultSet, int rowNum) throws SQLException {
            int index = 1;
            UUID idKorisnika = UUID.fromString(resultSet.getString(index++));
            String korisnickoIme = resultSet.getString(index++);
            String lozinka = resultSet.getString(index++);
            String email = resultSet.getString(index++);
            String ime = resultSet.getString(index++);
            String prezime = resultSet.getString(index++);
            LocalDate datumRodjenja = resultSet.getTimestamp(index++).toLocalDateTime().toLocalDate();
            String adresa = resultSet.getString(index++);
            String brojTelefona = resultSet.getString(index++);
            LocalDateTime datumIVremeRegistracije = resultSet.getTimestamp(index++).toLocalDateTime();
            Uloga uloga = Uloga.valueOf(resultSet.getString(index++));
            Boolean blokiran = resultSet.getBoolean(index++);
//            System.out.println("Blokiran kod ucitavanja je " +resultSet.getString(12));

            Korisnik korisnik = new Korisnik(idKorisnika, korisnickoIme, lozinka, email, ime, prezime, datumRodjenja, adresa,
                                            brojTelefona, datumIVremeRegistracije, uloga,blokiran);

            return korisnik;
        }
    }

    @Override
    public Korisnik findOne(UUID idKorisnika) {
        try {
            String sql = "SELECT * FROM korisnik WHERE idKorisnika = ?";
            return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), idKorisnika.toString());
        } catch (EmptyResultDataAccessException ex) {
            System.out.println("Nije nadjen korisnik sa idom = " + idKorisnika);
            //Vracamo null ako korisnik nije pronadjen
            return null;
        }
    }

    @Override
    public Korisnik findOne(String korisnickoIme) {
        try {
            String sql = "SELECT * FROM korisnik WHERE korisnickoIme = ?";
            return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), korisnickoIme);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public Korisnik findOne(String korisnickoIme, String lozinka) {
        try {
            String sql = "SELECT * FROM korisnik WHERE korisnickoIme = ? AND lozinka = ?";
            return  jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), korisnickoIme, lozinka);
        } catch (EmptyResultDataAccessException ex) {
            System.out.println("Nije nadjen korisnik sa korisnickim imenom = " + korisnickoIme + " i sifrom = " + lozinka);
            return null;
        }
    }

    @Override
    public List<Korisnik> findAll() {
        String sql = "SELECT * FROM korisnik";
        return jdbcTemplate.query(sql, new KorisnikRowMapper());
    }

    @Override
    public List<Korisnik> find(String korisnickoIme, Uloga uloga) {

        ArrayList<Object> listaArgumenata = new ArrayList<>();

        String sql = "SELECT * FROM korisnik";

        StringBuffer whereSql = new StringBuffer(" WHERE ");

        boolean imaArgumenata = false;


        if(korisnickoIme != null) {
            korisnickoIme = "%" + korisnickoIme + "%";
            if(imaArgumenata) {
                whereSql.append(" AND ");
            }
            whereSql.append("korisnickoIme LIKE ?");
            imaArgumenata = true;
            listaArgumenata.add(korisnickoIme);
        }

        if(uloga != null) {
            String ulogaStr = " " + uloga.toString() + " ";
            if (imaArgumenata) {
                whereSql.append(" AND ");
            }
            whereSql.append("uloga = ?");
            imaArgumenata = true;
            listaArgumenata.add(uloga.toString());
        }

        if (imaArgumenata) {
            sql = sql + whereSql.toString() + " ORDER BY korisnickoIme";
            System.out.println(sql);
        } else {
            sql = sql + " ORDER BY korisnickoIme";
            System.out.println(sql);
        }

        return jdbcTemplate.query(sql, listaArgumenata.toArray(), new KorisnikRowMapper());
    }

    @Override
    @Transactional
    public int save(Korisnik korisnik) {
        String sql = "INSERT INTO korisnik VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        return jdbcTemplate.update(sql, korisnik.getIdKorisnika().toString(), korisnik.getKorisnickoIme(), korisnik.getLozinka(),
                korisnik.getEmail(), korisnik.getIme(), korisnik.getPrezime(), korisnik.getDatumRodjenja(), korisnik.getAdresa(),
                korisnik.getBrojTelefona(), korisnik.getDatumIVremeRegistracije(), korisnik.getUloga().toString(), korisnik.isBlokiran());
    }

    @Override
    @Transactional
    public int update(Korisnik korisnik) {
        System.out.println("Radimo update za korisnika " + korisnik);
        if (korisnik.getLozinka() == null) {
            String sql = "UPDATE korisnik SET korisnickoIme = ?, email = ?, ime = ?, prezime = ?, datumRodjenja = ?," +
                    " adresa = ?, brojTelefona = ?, datumIVremeRegistracije = ?, uloga = ?, blokiran = ? WHERE idKorisnika = ?";
            int i = jdbcTemplate.update(sql, korisnik.getKorisnickoIme(), korisnik.getEmail(),
                                        korisnik.getIme(), korisnik.getPrezime(), korisnik.getDatumRodjenja(), korisnik.getAdresa(),
                                        korisnik.getBrojTelefona(), korisnik.getDatumIVremeRegistracije(), korisnik.getUloga().toString(),
                                        korisnik.isBlokiran(), korisnik.getIdKorisnika().toString());
            System.out.println(i);

            return i;
        } else {
            String sql = "UPDATE korisnik SET korisnickoIme = ?, lozinka = ?, email = ?, ime = ?, prezime = ?, datumRodjenja = ?," +
                    " adresa = ?, brojTelefona = ?, datumIVremeRegistracije = ?, uloga = ?, blokiran = ? WHERE idKorisnika = ?";
            int i = jdbcTemplate.update(sql, korisnik.getKorisnickoIme(), korisnik.getLozinka(), korisnik.getEmail(),
                    korisnik.getIme(), korisnik.getPrezime(), korisnik.getDatumRodjenja(), korisnik.getAdresa(),
                    korisnik.getBrojTelefona(), korisnik.getDatumIVremeRegistracije(), korisnik.getUloga().toString(),
                    korisnik.isBlokiran(), korisnik.getIdKorisnika().toString());

            System.out.println(i);

            return i;
        }
    }

    @Override
    @Transactional
    public int delete(UUID idKorisnika) {
        String sql = "DELETE FROM korisnik WHERE korisnickoIme = ?";
        return jdbcTemplate.update(sql, idKorisnika.toString());
    }


}
