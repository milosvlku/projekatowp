package com.ftn.ProjekatOWP.dao.impl;

import com.ftn.ProjekatOWP.dao.KnjigaDAO;
import com.ftn.ProjekatOWP.dao.ListaZeljaDAO;
import com.ftn.ProjekatOWP.dao.ListaZeljaKnjigaDAO;
import com.ftn.ProjekatOWP.models.Knjiga;
import com.ftn.ProjekatOWP.models.ListaZelja;
import com.ftn.ProjekatOWP.models.ListaZeljaKnjiga;
import com.ftn.ProjekatOWP.services.KnjigaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Repository
public class ListaZeljaKnjigaDAOImpl implements ListaZeljaKnjigaDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private KnjigaService knjigaService;

    @Autowired
    private ListaZeljaDAO listaZeljaDAO;

    private class ListaZeljaKnjigaMapper implements RowMapper<ListaZeljaKnjiga> {

        @Override
        public ListaZeljaKnjiga mapRow(ResultSet resultSet, int i) throws SQLException {
            int index = 1;
            UUID idListaZeljaKnjiga = UUID.fromString(resultSet.getString(index++));
            ListaZelja listaZelja = listaZeljaDAO.findOne(UUID.fromString(resultSet.getString(index++)));
            Knjiga knjiga = knjigaService.findOne(UUID.fromString(resultSet.getString(index++)));
            ListaZeljaKnjiga listaZeljaKnjiga = new ListaZeljaKnjiga(idListaZeljaKnjiga, listaZelja, knjiga);

            return listaZeljaKnjiga;
        }
    }


    @Override
    public ListaZeljaKnjiga findOne(UUID idListeZeljaKnjiga) {
        String sql = "SELECT * FROM lista_zelja_knjiga WHERE idListaZeljaKnjiga = ?";
        return jdbcTemplate.queryForObject(sql, new ListaZeljaKnjigaMapper(), idListeZeljaKnjiga.toString());
    }

    @Override
    public Set<ListaZeljaKnjiga> findByListaZeljaIdListeZelja(UUID idListaZelja) {
        String sql = "SELECT * FROM lista_zelja_knjiga WHERE listaZelja = ?";
        Set<ListaZeljaKnjiga> listaZeljaKnjigaSet = new HashSet<>();
        listaZeljaKnjigaSet.addAll(jdbcTemplate.query(sql, new ListaZeljaKnjigaMapper(), idListaZelja.toString()));
        return listaZeljaKnjigaSet;
    }

    @Override
    public ListaZeljaKnjiga findByListaZeljaIdListeZeljaAndKnjigaIdKnjige(UUID idListaZelja, UUID idKnjige) {
        String sql = "SELECT * FROM lista_zelja_knjiga WHERE listaZelja = ? AND knjiga = ?";
        return jdbcTemplate.queryForObject(sql, new ListaZeljaKnjigaMapper(), idListaZelja.toString(), idKnjige.toString());
    }

    @Transactional
    @Override
    public int save(ListaZeljaKnjiga listaZeljaKnjiga) {
        String sql = "INSERT INTO lista_zelja_knjiga VALUES (?,?,?)";
        return jdbcTemplate.update(sql,
                listaZeljaKnjiga.getIdListaZeljaKnjiga().toString(),
                listaZeljaKnjiga.getListaZelja().getIdListeZelja().toString(),
                listaZeljaKnjiga.getKnjiga().getIdKnjige().toString());
    }

    @Transactional
    @Override
    public int update(ListaZeljaKnjiga listaZeljaKnjiga) {
        String sql = "UPDATE lista_zelja_knjiga SET listaZelja = ?, knjiga = ? WHERE idListaZeljaKnjiga = ?";
        return jdbcTemplate.update(sql,
                listaZeljaKnjiga.getListaZelja().getIdListeZelja().toString(),
                listaZeljaKnjiga.getKnjiga().getIdKnjige().toString(),
                listaZeljaKnjiga.getIdListaZeljaKnjiga().toString());
    }

    @Transactional
    @Override
    public int delete(UUID idListaZeljaKnjiga) {
        String sql = "DELETE FROM lista_zelja_knjiga WHERE idListaZeljaKnjiga = ?";
        return jdbcTemplate.update(sql, idListaZeljaKnjiga.toString());
    }
}
