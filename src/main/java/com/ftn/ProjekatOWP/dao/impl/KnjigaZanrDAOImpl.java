package com.ftn.ProjekatOWP.dao.impl;

import com.ftn.ProjekatOWP.dao.KnjigaDAO;
import com.ftn.ProjekatOWP.dao.KnjigaZanrDAO;
import com.ftn.ProjekatOWP.dao.ZanrDAO;
import com.ftn.ProjekatOWP.models.Knjiga;
import com.ftn.ProjekatOWP.models.KnjigaZanr;
import com.ftn.ProjekatOWP.models.Zanr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@Repository
public class KnjigaZanrDAOImpl implements KnjigaZanrDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private KnjigaDAO knjigaDAO;

    @Autowired
    private ZanrDAO zanrDAO;

    private class KnjigaZanrMapper implements RowMapper<KnjigaZanr> {
        @Override
        public KnjigaZanr mapRow(ResultSet resultSet, int i) throws SQLException {
            int index = 1;
            UUID idKnjigeZanra = UUID.fromString(resultSet.getString(index++));
            Knjiga knjiga = knjigaDAO.findOne(UUID.fromString(resultSet.getString(index++)));
            Zanr zanr = zanrDAO.findOne(UUID.fromString(resultSet.getString(index++)));

            KnjigaZanr knjigaZanr = new KnjigaZanr(idKnjigeZanra, knjiga, zanr);

            return knjigaZanr;
        }
    }

    @Override
    public KnjigaZanr findOne(UUID idKnjigaZanr) {
        String sql = "SELECT * FROM knjiga_zanr WHERE idKnjigeZanr = ?";
        return jdbcTemplate.queryForObject(sql, new KnjigaZanrMapper(), idKnjigaZanr.toString());
    }

    @Override
    public List<KnjigaZanr> findAll() {
        String sql = "SELECT * FROM knjiga_zanr";
        return jdbcTemplate.query(sql, new KnjigaZanrMapper());
    }

    @Override
    public List<KnjigaZanr> findByKnjiga(UUID idKnjige) {
        String sql = "SELECT * FROM knjiga_zanr WHERE idKnjige = ?";
        return jdbcTemplate.query(sql, new KnjigaZanrMapper(), idKnjige.toString());
    }

    @Override
    public List<KnjigaZanr> findByZanr(UUID idZanra) {
        String sql = "SELECT * FROM knjiga_zanr WHERE idZanra = ?";
        return jdbcTemplate.query(sql, new KnjigaZanrMapper(), idZanra.toString());
    }

    @Override
    public KnjigaZanr findByKnjigaZanr(UUID idKnjige, UUID idZanra) {
        String sql = "SELECT * FROM knjiga_zanr WHERE idKnjige = ? AND idZanra = ?";
        return jdbcTemplate.queryForObject(sql, new KnjigaZanrMapper(), idKnjige.toString(), idZanra.toString());
    }

    @Override
    @Transactional
    public int save(KnjigaZanr knjigaZanr) {
        String sql = "INSERT INTO knjiga_zanr VALUES (?, ?, ?)";
        return jdbcTemplate.update(sql, knjigaZanr.getIdKnjigeZanr().toString(),
                                    knjigaZanr.getKnjiga().getIdKnjige().toString(),
                                    knjigaZanr.getZanr().getIdZanra().toString());
    }

    @Override
    @Transactional
    public int update(KnjigaZanr knjigaZanr) {
        String sql = "UPDATE knjiga_zanr SET idKnjige = ?, idZanra = ? WHERE idKnjigeZanr = ?";
        return jdbcTemplate.update(sql, knjigaZanr.getKnjiga().getIdKnjige().toString(),
                                        knjigaZanr.getZanr().getIdZanra().toString(),
                                        knjigaZanr.getIdKnjigeZanr().toString());
    }

    @Override
    @Transactional
    public int delete(UUID idKnjigaZanr) {
        String sql = "DELETE FROM knjiga_zanr WHERE idKnjigeZanr = ?";
        return jdbcTemplate.update(sql, idKnjigaZanr.toString());
    }
}
