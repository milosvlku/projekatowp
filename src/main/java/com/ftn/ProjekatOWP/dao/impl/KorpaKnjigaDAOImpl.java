package com.ftn.ProjekatOWP.dao.impl;

import com.ftn.ProjekatOWP.dao.KnjigaDAO;
import com.ftn.ProjekatOWP.dao.KorpaDAO;
import com.ftn.ProjekatOWP.dao.KorpaKnjigaDAO;
import com.ftn.ProjekatOWP.models.Knjiga;
import com.ftn.ProjekatOWP.models.Korpa;
import com.ftn.ProjekatOWP.models.KorpaKnjiga;
import com.ftn.ProjekatOWP.services.KnjigaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Repository
public class KorpaKnjigaDAOImpl implements KorpaKnjigaDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private KorpaDAO korpaDAO;

    @Autowired
    private KnjigaService knjigaService;

    private class KorpaKnjigaMapper implements RowMapper<KorpaKnjiga> {

        @Override
        public KorpaKnjiga mapRow(ResultSet resultSet, int i) throws SQLException {
            int index = 1;
            UUID idKorpeKnjige = UUID.fromString(resultSet.getString(index++));
            Korpa korpa = korpaDAO.findOne(UUID.fromString(resultSet.getString(index++)));
            Knjiga knjiga = knjigaService.findOne(UUID.fromString(resultSet.getString(index++)));
            int kolicina = resultSet.getInt(index++);

            KorpaKnjiga korpaKnjiga = new KorpaKnjiga(idKorpeKnjige, korpa, knjiga, kolicina);

            return korpaKnjiga;
        }
    }

    @Override
    public KorpaKnjiga findOne(UUID idKorpeKnjige) {
        String sql = "SELECT * FROM korpa_knjiga WHERE idKorpeKnjige = ?";
        return jdbcTemplate.queryForObject(sql, new KorpaKnjigaMapper(), idKorpeKnjige.toString());
    }

    @Override
    public List<KorpaKnjiga> findByKorpaIdKorpe(UUID idKorpe) {
        String sql = "SELECT * FROM korpa_knjiga WHERE korpa = ?";
        return jdbcTemplate.query(sql, new KorpaKnjigaMapper(), idKorpe.toString());
    }

    @Override
    public List<KorpaKnjiga> findByKnjigaIdKnjige(UUID idKnjige) {
        String sql = "SELECT * FROM korpa_knjiga WHERE knjiga = ?";
        return jdbcTemplate.query(sql, new KorpaKnjigaMapper(), idKnjige.toString());
    }



    @Override
    public int save(KorpaKnjiga korpaKnjiga) {
        String sql = "INSERT INTO korpa_knjiga VALUES (?, ?, ?, ?)";
        return jdbcTemplate.update(sql,
                korpaKnjiga.getIdKorpeKnjige().toString(),
                korpaKnjiga.getKorpa().getIdKorpe().toString(),
                korpaKnjiga.getKnjiga().getIdKnjige().toString(),
                korpaKnjiga.getKolicina());
    }

    @Override
    public int update(KorpaKnjiga korpaKnjiga) {
        String sql = "UPDATE korpa_knjiga SET korpa = ?, knjiga = ?, kolicina = ? WHERE idKorpeKnjige = ?";
        return jdbcTemplate.update(sql,
                korpaKnjiga.getKorpa().getIdKorpe().toString(),
                korpaKnjiga.getKnjiga().getIdKnjige().toString(),
                korpaKnjiga.getKolicina(),
                korpaKnjiga.getIdKorpeKnjige().toString());
    }

    @Override
    public int delete(UUID idKorpeKnjige) {
        String sql = "DELETE FROM korpa_knjiga WHERE idKorpeKnjige = ?";
        return jdbcTemplate.update(sql, idKorpeKnjige.toString());
    }

    @Override
    public KorpaKnjiga findByKorpaIdKorpeAndKnjigaIdKnjige(UUID idKorpe, UUID idKnjige) {
        String sql = "SELECT * FROM korpa_knjiga WHERE korpa = ? AND knjiga = ?";
        return jdbcTemplate.queryForObject(sql, new KorpaKnjigaMapper(), idKorpe.toString(), idKnjige.toString());
    }
}
