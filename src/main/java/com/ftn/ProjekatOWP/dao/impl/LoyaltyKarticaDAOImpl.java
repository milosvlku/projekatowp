package com.ftn.ProjekatOWP.dao.impl;

import com.ftn.ProjekatOWP.dao.KorisnikDAO;
import com.ftn.ProjekatOWP.dao.LoyaltyKarticaDAO;
import com.ftn.ProjekatOWP.enums.Status;
import com.ftn.ProjekatOWP.models.Korisnik;
import com.ftn.ProjekatOWP.models.LoyaltyKartica;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@Repository
public class LoyaltyKarticaDAOImpl implements LoyaltyKarticaDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private KorisnikDAO korisnikDAO;

    private class LoyaltyRowMapper implements RowMapper<LoyaltyKartica> {
        @Override
        public LoyaltyKartica mapRow(ResultSet resultSet, int i) throws SQLException {
            int index = 1;
            UUID idLoyaltyKartice = UUID.fromString(resultSet.getString(index++));
            Korisnik korisnik = korisnikDAO.findOne(UUID.fromString(resultSet.getString(index++)));
            int brojPoena = resultSet.getInt(index++);
            Status status = Status.valueOf(resultSet.getString(index++));

            LoyaltyKartica loyaltyKartica = new LoyaltyKartica(idLoyaltyKartice, korisnik, brojPoena, status);

            return loyaltyKartica;
        }
    }

    @Override
    public List<LoyaltyKartica> findAll() {
        String sql = "SELECT * FROM loyalty_kartica";
        return jdbcTemplate.query(sql, new LoyaltyRowMapper());
    }

    @Override
    public LoyaltyKartica findOne(UUID idLoyaltyKartice) {
        String sql = "SELECT * FROM loyalty_kartica WHERE idLoyaltyKartice = ?";
        return jdbcTemplate.queryForObject(sql, new LoyaltyRowMapper(), idLoyaltyKartice);
    }

    @Override
    public LoyaltyKartica findByKorisnik(UUID idKorisnika) {
        String sql = "SELECT * FROM loyalty_kartica WHERE korisnik = ?";
        LoyaltyKartica loyaltyKartica = null;
        try {
            loyaltyKartica = jdbcTemplate.queryForObject(sql, new LoyaltyRowMapper(), idKorisnika.toString());
        } catch (EmptyResultDataAccessException ex) {
            loyaltyKartica = null;
        }
        return loyaltyKartica;
    }

    @Override
    public List<LoyaltyKartica> findByStatus(Status status) {
        String sql = "SELECT * FROM loyalty_kartica WHERE status = ?";
        return jdbcTemplate.query(sql, new LoyaltyRowMapper(), status.toString());
    }

    @Override
    public int save(LoyaltyKartica loyaltyKartica) {
        String sql = "INSERT INTO loyalty_kartica VALUES (?,?,?,?)";
        return jdbcTemplate.update(sql, loyaltyKartica.getIdLoyaltyKartice().toString(),
                loyaltyKartica.getKorisnik().getIdKorisnika().toString(),
                loyaltyKartica.getBrojPoena(), loyaltyKartica.getStatusKartice().toString());
    }

    @Override
    public int update(LoyaltyKartica loyaltyKartica) {
        String sql = "UPDATE loyalty_kartica SET korisnik = ?, brojPoena = ?, status = ? WHERE idLoyaltyKartice = ?";
        return jdbcTemplate.update(sql, loyaltyKartica.getKorisnik().getIdKorisnika().toString(),
                loyaltyKartica.getBrojPoena(), loyaltyKartica.getStatusKartice().toString(),
                loyaltyKartica.getIdLoyaltyKartice().toString());
    }

    @Override
    public int delete(LoyaltyKartica loyaltyKartica) {
        String sql = "DELETE FROM loyalty_kartica WHERE idLoyaltyKartice = ?";
        return jdbcTemplate.update(sql, loyaltyKartica.getIdLoyaltyKartice().toString());
    }
}
