package com.ftn.ProjekatOWP.dao.impl;

import com.ftn.ProjekatOWP.dao.KorisnikDAO;
import com.ftn.ProjekatOWP.dao.KorpaDAO;
import com.ftn.ProjekatOWP.models.Korisnik;
import com.ftn.ProjekatOWP.models.Korpa;
import com.ftn.ProjekatOWP.models.KorpaKnjiga;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

@Repository
public class KorpaDAOImpl implements KorpaDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

//    @Autowired
//    private KorpaKnjigaDAO korpaKnjigaDAO;

    @Autowired
    private KorisnikDAO korisnikDAO;

    private class KorpaRowMapper implements RowMapper<Korpa> {

        @Override
        public Korpa mapRow(ResultSet resultSet, int i) throws SQLException {
            int index = 1;
            UUID idKorpe = UUID.fromString(resultSet.getString(index++));
            Korisnik korisnik = korisnikDAO.findOne(UUID.fromString(resultSet.getString(index++)));

            Korpa korpa = new Korpa(idKorpe, korisnik);

            return korpa;
        }
    }


    @Override
    public Korpa findOne(UUID idKorpe) {
        String sql = "SELECT * FROM korpa WHERE idKorpe = ?";
        return jdbcTemplate.queryForObject(sql, new KorpaRowMapper(), idKorpe.toString());
    }

    @Override
    public Korpa findKorpaByKorisnikIdKorisnika(UUID idKorisnika) {
        String sql = "SELECT * FROM korpa WHERE korisnik = ?";
        Korpa korpa = null;
        try {
            korpa = jdbcTemplate.queryForObject(sql, new KorpaRowMapper(), idKorisnika.toString());
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
        return korpa;
    }

    @Transactional
    @Override
    public int save(Korpa korpa) {
        String sql = "INSERT INTO korpa VALUES (?,?)";
        return jdbcTemplate.update(sql, korpa.getIdKorpe().toString(), korpa.getKorisnik().getIdKorisnika().toString());
    }

    @Transactional
    @Override
    public int update(Korpa korpa) {
        boolean uspeh = true;
        String sql = "DELETE FROM korpa_knjiga WHERE korpa = ?";
        uspeh = jdbcTemplate.update(sql, korpa.getIdKorpe().toString()) >= 0;

        sql = "INSERT INTO korpa_knjiga VALUE (?,?,?,?)";
        for(KorpaKnjiga korpaKnjiga:korpa.getStavke()) {
            uspeh = uspeh && jdbcTemplate.update(sql, UUID.randomUUID().toString(),
                    korpaKnjiga.getKorpa().getIdKorpe().toString(),
                    korpaKnjiga.getKnjiga().getIdKnjige().toString(),
                    korpaKnjiga.getKolicina()) == 1;
        }

        sql = "UPDATE korpa SET korisnik = ? WHERE idKorpe = ?";
        uspeh = uspeh && jdbcTemplate.update(sql,
                korpa.getKorisnik().getIdKorisnika().toString(),
                korpa.getIdKorpe().toString()) == 1;

        return uspeh?1:0;
    }

    @Transactional
    @Override
    public int delete(UUID idKorpe) {
        String sql = "DELETE FROM korpa WHERE idKorpe = ?";
        return jdbcTemplate.update(sql, idKorpe.toString());
    }
}
