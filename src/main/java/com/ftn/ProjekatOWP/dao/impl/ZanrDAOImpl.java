package com.ftn.ProjekatOWP.dao.impl;

import com.ftn.ProjekatOWP.dao.ZanrDAO;
import com.ftn.ProjekatOWP.models.Zanr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
public class ZanrDAOImpl implements ZanrDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class ZanrRowMapper implements RowMapper<Zanr> {

        @Override
        public Zanr mapRow(ResultSet rs, int rowNum) throws SQLException {
            int index = 1;
            UUID id = UUID.fromString(rs.getString(index++));
            String naziv = rs.getString(index++);
            String opis = rs.getString(index++);

            Zanr zanr = new Zanr(id, naziv, opis);
            return zanr;
        }
    }

    @Override
    public Zanr findOne(UUID idZanra) {
        String sql = "SELECT * FROM zanr WHERE idZanra= ?";
        return jdbcTemplate.queryForObject(sql, new ZanrRowMapper(), idZanra.toString());
    }

    @Override
    public List<Zanr> findAll() {
        String sql = "SELECT * FROM zanr";
        return jdbcTemplate.query(sql, new ZanrRowMapper());
    }

    @Override
    public List<Zanr> find(String naziv) {
        naziv = "%" + naziv + "%";
        String sql = "SELECT * FROM zanr WHERE naziv LIKE ?";
        return jdbcTemplate.query(sql, new ZanrRowMapper(), naziv);
    }

    @Override
    public int save(Zanr zanr) {
        String sql = "INSERT INTO zanr (idZanra, naziv, opis) VALUES (?, ?, ?)";
        return jdbcTemplate.update(sql, zanr.getIdZanra().toString(), zanr.getNaziv(), zanr.getOpis());
    }

    @Override
    public int[] save(ArrayList<Zanr> zanrovi) {
        String sql = "INSERT INTO zanr (idZanra, naziv, opis) VALUES (?,?,?)";
        return jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
                preparedStatement.setString(1, zanrovi.get(i).getIdZanra().toString());
                preparedStatement.setString(2, zanrovi.get(i).getNaziv());
                preparedStatement.setString(3, zanrovi.get(i).getOpis());
            }

            @Override
            public int getBatchSize() {
                return zanrovi.size();
            }
        });
    }

    @Override
    @Transactional
    public int update(Zanr zanr) {
        String sql = "UPDATE zanr SET naziv = ?, opis = ? WHERE idZanra = ?";
        return jdbcTemplate.update(sql, zanr.getNaziv(), zanr.getOpis(), zanr.getIdZanra().toString());
    }

    @Override
    @Transactional
    public int delete(UUID idZanra) {
        String sql = "DELETE FROM zanr WHERE idZanra = ?";
        return jdbcTemplate.update(sql, idZanra.toString());
    }


}
