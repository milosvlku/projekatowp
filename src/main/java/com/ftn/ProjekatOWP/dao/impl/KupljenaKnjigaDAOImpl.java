package com.ftn.ProjekatOWP.dao.impl;

import com.ftn.ProjekatOWP.dao.KnjigaDAO;
import com.ftn.ProjekatOWP.dao.KupljenaKnjigaDAO;
import com.ftn.ProjekatOWP.dao.KupovinaDAO;
import com.ftn.ProjekatOWP.models.Knjiga;
import com.ftn.ProjekatOWP.models.KupljenaKnjiga;
import com.ftn.ProjekatOWP.models.Kupovina;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@Repository
public class KupljenaKnjigaDAOImpl implements KupljenaKnjigaDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private KupovinaDAO kupovinaDAO;

    @Autowired
    private KnjigaDAO knjigaDAO;

    private class KupljenaKnjigaRowMapper implements RowMapper<KupljenaKnjiga> {

        @Override
        public KupljenaKnjiga mapRow(ResultSet resultSet, int i) throws SQLException {
            int index = 1;
            UUID idKupovineKnjige = UUID.fromString(resultSet.getString(index++));
            Kupovina kupovina = kupovinaDAO.findOne(UUID.fromString(resultSet.getString(index++)));
            Knjiga knjiga = knjigaDAO.findOne(UUID.fromString(resultSet.getString(index++)));
            int brojPrimeraka = resultSet.getInt(index++);

            KupljenaKnjiga kupljenaKnjiga = new KupljenaKnjiga(idKupovineKnjige, kupovina, knjiga, brojPrimeraka);

            return kupljenaKnjiga;
        }
    }

    @Override
    public KupljenaKnjiga findOne(UUID idKupovineKnjige) {
        String sql = "SELECT * FROM kupljena_knjiga WHERE idKupovineKnjige = ?";
        return jdbcTemplate.queryForObject(sql, new KupljenaKnjigaRowMapper(), idKupovineKnjige.toString());
    }

    @Override
    public List<KupljenaKnjiga> findAll() {
        String sql = "SELECT * FROM kupljena_knjiga";
        return jdbcTemplate.query(sql, new KupljenaKnjigaRowMapper());
    }

    @Override
    public List<KupljenaKnjiga> findAllByKupovinaIdKupovine(UUID idKupovine) {
        String sql = "SELECT * FROM kupljena_knjiga WHERE kupovina = ?";
        return jdbcTemplate.query(sql, new KupljenaKnjigaRowMapper(), idKupovine.toString());
    }

    @Transactional
    @Override
    public int save(KupljenaKnjiga kupljenaKnjiga) {
        String sql = "INSERT INTO kupljena_knjiga VALUES (?,?,?,?)";
        return jdbcTemplate.update(sql, kupljenaKnjiga.getIdKupovineKnjige().toString(),
                kupljenaKnjiga.getKupovina().getIdKupovine().toString(),
                kupljenaKnjiga.getKnjiga().getIdKnjige().toString(),
                kupljenaKnjiga.getBrojPrimeraka());
    }

    @Transactional
    @Override
    public int update(KupljenaKnjiga kupljenaKnjiga) {
        String sql = "UPDATE kupljena_knjiga SET kupovina = ?, knjiga = ?, brojPrimeraka = ? WHERE idKupovineKnjige  = ?";
        return jdbcTemplate.update(sql, kupljenaKnjiga.getKupovina().getIdKupovine().toString(),
                kupljenaKnjiga.getKnjiga().getIdKnjige().toString(),
                kupljenaKnjiga.getBrojPrimeraka());
    }

    @Transactional
    @Override
    public int delete(UUID idKupovineKnjige) {
        String sql = "DELETE FROM kupljena_knjiga WHERE idKupovineKnjige = ?";
        return jdbcTemplate.update(sql, idKupovineKnjige.toString());
    }
}
