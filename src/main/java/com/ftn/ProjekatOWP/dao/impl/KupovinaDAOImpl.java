package com.ftn.ProjekatOWP.dao.impl;

import com.ftn.ProjekatOWP.dao.KorisnikDAO;
import com.ftn.ProjekatOWP.dao.KupovinaDAO;
import com.ftn.ProjekatOWP.models.Korisnik;
import com.ftn.ProjekatOWP.models.Kupovina;
import com.ftn.ProjekatOWP.models.Zanr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
public class KupovinaDAOImpl implements KupovinaDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private KorisnikDAO korisnikDAO;

    private class KupovinaRowMapper implements RowMapper<Kupovina> {

        @Override
        public Kupovina mapRow(ResultSet resultSet, int i) throws SQLException {
            int index = 1;
            UUID idKupovine = UUID.fromString(resultSet.getString(index++));
            LocalDate datumKupovine = resultSet.getTimestamp(index++).toLocalDateTime().toLocalDate();
            Korisnik korisnik = korisnikDAO.findOne(UUID.fromString(resultSet.getString(index++)));
            int brojPotrosenihPoena = resultSet.getInt(index++);

            Kupovina kupovina = new Kupovina(idKupovine, datumKupovine, korisnik, brojPotrosenihPoena);

            return kupovina;
        }
    }

    @Override
    public Kupovina findOne(UUID idKupovine) {
        String sql = "SELECT * FROM kupovina where idKupovine = ?";
        return jdbcTemplate.queryForObject(sql, new KupovinaRowMapper(), idKupovine.toString());
    }

    @Override
    public List<Kupovina> findAll() {
        String sql = "SELECT * FROM kupovina";
        return jdbcTemplate.query(sql, new KupovinaRowMapper());
    }

    @Override
    public List<Kupovina> findByKorisnikIdKorisnika(UUID korisnik) {
        String sql = "SELECT * FROM kupovina WHERE korisnik = ?";
        return jdbcTemplate.query(sql, new KupovinaRowMapper(), korisnik.toString());
    }

    @Transactional
    @Override
    public int save(Kupovina kupovina) {
        String sql = "INSERT INTO kupovina values(?,?,?,?);";
        return jdbcTemplate.update(sql, kupovina.getIdKupovine().toString(),
                kupovina.getDatumKupovine(), kupovina.getKorisnik().getIdKorisnika().toString(), kupovina.getBrojUtrosenihPoena());
    }

    @Transactional
    @Override
    public int update(Kupovina kupovina) {
        String sql = "UPDATE kupovina SET datumKupovine = ?, korisnik = ?, brojPotrosenihPoena = ? WHERE idKupovine = ?";
        return jdbcTemplate.update(sql, kupovina.getDatumKupovine(), kupovina.getKorisnik().getIdKorisnika(),
                kupovina.getIdKupovine().toString());
    }

    @Transactional
    @Override
    public int delete(UUID idKupovine) {
        String sql = "DELETE FROM kupovina WHERE idKupovine = ?";
        return jdbcTemplate.update(sql, idKupovine.toString());
    }
}
