package com.ftn.ProjekatOWP.dao.impl;

import com.ftn.ProjekatOWP.dao.KnjigaDAO;
import com.ftn.ProjekatOWP.dao.KomentarDAO;
import com.ftn.ProjekatOWP.dao.KorisnikDAO;
import com.ftn.ProjekatOWP.enums.Status;
import com.ftn.ProjekatOWP.models.Knjiga;
import com.ftn.ProjekatOWP.models.Komentar;
import com.ftn.ProjekatOWP.models.Korisnik;
import com.ftn.ProjekatOWP.services.KnjigaService;
import com.ftn.ProjekatOWP.services.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Repository
public class KomentarDAOImpl implements KomentarDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private KorisnikDAO korisnikService;

    @Autowired
    private KnjigaDAO knjigaDAO;


    private class KomentarRowMapper implements RowMapper<Komentar> {

        @Override
        public Komentar mapRow(ResultSet resultSet, int i) throws SQLException {
            int index = 1;
            UUID id = UUID.fromString(resultSet.getString(index++));
            String tekstKomentara = resultSet.getString(index++);
            int ocena = resultSet.getInt(index++);
            LocalDate datumKomentara = resultSet.getDate(index++).toLocalDate();
            //Korisnik
            Korisnik korisnik = korisnikService.findOne(UUID.fromString(resultSet.getString(index++)));
            //Knjiga
            Knjiga knjiga = knjigaDAO.findOne(UUID.fromString(resultSet.getString(index++)));
            Status status = Status.valueOf(resultSet.getString(index++));

            Komentar komentar = new Komentar(id, tekstKomentara, ocena, datumKomentara, korisnik, knjiga, status);
            return komentar;
        }
    }

    @Override
    public Komentar findOne(UUID idKomentara) {
        String sql = "SELECT * FROM komentar WHERE idKomentara = ?";
        return jdbcTemplate.queryForObject(sql, new KomentarRowMapper(), idKomentara.toString());
    }

    @Override
    public List<Komentar> findAll() {
        String sql = "SELECT * FROM komentar";
        return jdbcTemplate.query(sql, new KomentarRowMapper());
    }

    @Override
    public List<Komentar> findByKnjiga(UUID idKnjige) {
        String sql = "SELECT * FROM komentar WHERE knjiga = ?";
        return jdbcTemplate.query(sql, new KomentarRowMapper(), idKnjige.toString());
    }

    @Override
    public List<Komentar> findByAutor(UUID idAutora) {
        String sql = "SELECT * FROM komentar WHERE autorKomentara = ?";
        return jdbcTemplate.query(sql, new KomentarRowMapper(), idAutora.toString());
    }

    @Override
    public Komentar findByKnjigaAutor(UUID idKnjige, UUID idAutora) {
        String sql = "SELECT * FROM komentar WHERE knjiga = ? AND autorKomentara = ?";
        Komentar komentar = null;
        try {
            komentar = jdbcTemplate.queryForObject(sql, new KomentarRowMapper(), idKnjige.toString(), idAutora.toString());
        } catch (EmptyResultDataAccessException ex) {
            komentar = null;
        }
        return komentar;
    }

    @Override
    @Transactional
    public int save(Komentar komenatar) {
        String sql = "INSERT INTO komentar VALUES (?, ?, ?, ?, ?, ?, ?)";
        return jdbcTemplate.update(sql, komenatar.getIdKomentara().toString(), komenatar.getTekstKomentara(),
                komenatar.getOcena(), komenatar.getDatumKomentara(), komenatar.getAutorKomentara().getIdKorisnika().toString(),
                komenatar.getKnjiga().getIdKnjige().toString(), komenatar.getStatus().toString());
    }

    @Override
    @Transactional
    public int update(Komentar komentar) {
        String sql = "UPDATE komentar SET tekstKomentara = ?, ocena = ?, datumKomentara = ?, autorKomentara = ?, knjiga = ?, status = ? WHERE idKomentara = ?";
        return jdbcTemplate.update(sql, komentar.getTekstKomentara(), komentar.getOcena(),
                komentar.getDatumKomentara(), komentar.getAutorKomentara().getIdKorisnika().toString(),
                komentar.getKnjiga().getIdKnjige().toString(), komentar.getStatus().toString(),
                komentar.getIdKomentara().toString());
    }

    @Override
    @Transactional
    public int delete(UUID idKomentara) {
        String sql = "DELETE FROM komentar WHERE idKomentara = ?";
        return jdbcTemplate.update(sql, idKomentara.toString());
    }
}
