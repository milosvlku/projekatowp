package com.ftn.ProjekatOWP.dao.impl;

import com.ftn.ProjekatOWP.dao.AutorDAO;
import com.ftn.ProjekatOWP.models.Autor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
public class AutorDAOImpl implements AutorDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class AutorRowMapper implements RowMapper<Autor> {
        @Override
        public Autor mapRow(ResultSet rs, int rowNum) throws SQLException {
            int index = 1;
            UUID idAutora = UUID.fromString(rs.getString(index++));
            String ime = rs.getString(index++);
            String prezime = rs.getString(index++);

            Autor autor = new Autor(idAutora, ime, prezime);
            return autor;
        }
    }

    @Override
    public Autor findOne(UUID idAutora) {
        String sql = "SELECT * FROM autor WHERE idAutora = ?";
        return jdbcTemplate.queryForObject(sql, new AutorRowMapper(), idAutora.toString());
    }

    @Override
    public Autor findOneByImePrezime(String imeAutora, String prezimeAutora) {
        Autor autor = null;
        String sql = "SELECT * FROM autor WHERE imeAutora = ? AND prezimeAutora = ?";
        try {
            autor = jdbcTemplate.queryForObject(sql, new AutorRowMapper(), imeAutora.trim(), prezimeAutora.trim());
        } catch (EmptyResultDataAccessException erdae) {
            System.out.println("dal me jebete");
            autor = new Autor(UUID.randomUUID(), imeAutora, prezimeAutora);
            save(autor);
            System.out.println("Novi autor kreiran " +autor);
        }

        return autor;
    }

    @Override
    public List<Autor> findAll() {
        String sql = "SELECT * FROM autor";
        return jdbcTemplate.query(sql, new AutorRowMapper());
    }

    @Transactional
    @Override
    public int save(Autor autor) {
        String sql = "INSERT INTO autor VALUES (?,?,?)";
        return jdbcTemplate.update(sql, autor.getIdAutora().toString(), autor.getImeAutora(), autor.getPrezimeAutora());
    }

    /*@Override
    public int[] save(ArrayList<Autor> autori) {
        return new int[0];
    }*/

    @Transactional
    @Override
    public int update(Autor autor) {
        String sql = "UPDATE autor SET imeAutora = ?, prezimeAutora = ? WHERE idAutora = ?";
        return jdbcTemplate.update(sql, autor.getImeAutora(), autor.getPrezimeAutora(), autor.getIdAutora().toString());
    }

    @Transactional
    @Override
    public int delete(UUID idAutora) {
        String sql = "DELETE FROM autor WHERE idAutora = ?";
        return jdbcTemplate.update(sql, idAutora.toString());
    }
}
