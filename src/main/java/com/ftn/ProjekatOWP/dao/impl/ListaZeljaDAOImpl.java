package com.ftn.ProjekatOWP.dao.impl;

import com.ftn.ProjekatOWP.dao.KorisnikDAO;
import com.ftn.ProjekatOWP.dao.ListaZeljaDAO;
import com.ftn.ProjekatOWP.models.Knjiga;
import com.ftn.ProjekatOWP.models.Korisnik;
import com.ftn.ProjekatOWP.models.ListaZelja;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.JDBCType;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

@Repository
public class ListaZeljaDAOImpl implements ListaZeljaDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private KorisnikDAO korisnikDAO;

    private class ListaZeljaRowMapper implements RowMapper<ListaZelja> {

        @Override
        public ListaZelja mapRow(ResultSet resultSet, int i) throws SQLException {
            int index = 1;
            UUID idListeZelja = UUID.fromString(resultSet.getString(index++));
            Korisnik korisnik = korisnikDAO.findOne(UUID.fromString(resultSet.getString(index++)));
            ListaZelja listaZelja = new ListaZelja(idListeZelja, korisnik);
            return listaZelja;
        }
    }

    @Override
    public ListaZelja findOne(UUID idListeZelja) {
        String sql = "SELECT * FROM lista_zelja WHERE idListeZelja = ?";
        return jdbcTemplate.queryForObject(sql, new ListaZeljaRowMapper(), idListeZelja.toString());
    }

    @Transactional
    @Override
    public ListaZelja findListaZeljaByKorisnikIdKorisnika(UUID idKorisnika) {
        String sql = "SELECT * FROM lista_zelja WHERE korisnik = ?";
        ListaZelja listaZelja;
        try {
            listaZelja = jdbcTemplate.queryForObject(sql, new ListaZeljaRowMapper(), idKorisnika.toString());
        } catch (EmptyResultDataAccessException ex){
            listaZelja = new ListaZelja(UUID.randomUUID(),korisnikDAO.findOne(idKorisnika));
            save(listaZelja);
        }
        return listaZelja;
    }

    @Transactional
    @Override
    public int save(ListaZelja listaZelja) {
        String sql = "INSERT INTO lista_zelja VALUES (?,?)";
        return jdbcTemplate.update(sql, listaZelja.getIdListeZelja().toString(),
                listaZelja.getKorisnik().getIdKorisnika().toString());
    }

    @Transactional
    @Override
    public int update(ListaZelja listaZelja) {

        String sql = "DELETE FROM lista_zelja_knjiga WHERE listaZelja = ?";
        jdbcTemplate.update(sql, listaZelja.getIdListeZelja().toString());

        boolean uspeh = true;
        sql = "INSERT INTO lista_zelja_knjiga VALUES (?,?,?)";
        for(Knjiga knjiga:listaZelja.getKnjige()) {
            String idListeZelja = listaZelja.getIdListeZelja().toString();
            String idKnjige = knjiga.getIdKnjige().toString();
            String uuid = UUID.randomUUID().toString();
            uspeh = uspeh && jdbcTemplate.update(sql, UUID.randomUUID().toString(), listaZelja.getIdListeZelja().toString(), knjiga.getIdKnjige().toString()) == 1;
        }

        sql = "UPDATE lista_zelja SET korisnik = ? WHERE idListeZelja = ?";
        uspeh = uspeh && jdbcTemplate.update(sql, listaZelja.getKorisnik().getIdKorisnika().toString(), listaZelja.getIdListeZelja().toString()) == 1;

        return uspeh?1:0;
    }

    @Transactional
    @Override
    public int delete(UUID idListeZelja) {
        String sql = "DELETE FROM lista_zelja_knjiga WHERE listaZelja = ?";
        int brojObrisanihKnjiga = jdbcTemplate.update(sql, idListeZelja.toString());
        System.out.println("Broj obrisanih knjiga iz liste zelja " + brojObrisanihKnjiga);
        sql = "DELETE FROM lista_zelja WHERE idListeZelja = ?";
        return jdbcTemplate.update(sql, idListeZelja.toString()) + brojObrisanihKnjiga;

    }
}
