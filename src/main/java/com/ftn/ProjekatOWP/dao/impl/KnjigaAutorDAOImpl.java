package com.ftn.ProjekatOWP.dao.impl;

import com.ftn.ProjekatOWP.dao.AutorDAO;
import com.ftn.ProjekatOWP.dao.KnjigaAutorDAO;
import com.ftn.ProjekatOWP.dao.KnjigaDAO;
import com.ftn.ProjekatOWP.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@Repository
public class KnjigaAutorDAOImpl implements KnjigaAutorDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private KnjigaDAO knjigaDAO;

    @Autowired
    private AutorDAO autorDAO;

    private class KnjigaAutorMapper implements RowMapper<KnjigaAutor> {
        @Override
        public KnjigaAutor mapRow(ResultSet resultSet, int i) throws SQLException {
            int index = 1;
            UUID idKnjigeAutora = UUID.fromString(resultSet.getString(index++));
            Knjiga knjiga = knjigaDAO.findOne(UUID.fromString(resultSet.getString(index++)));
            Autor autor = autorDAO.findOne(UUID.fromString(resultSet.getString(index++)));

            KnjigaAutor knjigaAutor = new KnjigaAutor(idKnjigeAutora, knjiga, autor);

            return knjigaAutor;
        }
    }

    @Override
    public KnjigaAutor findOne(UUID idKnjigeAutora) {
        String sql = "SELECT * FROM knjiga_autor WHERE idKnjigeAutora = ?";
        return jdbcTemplate.queryForObject(sql, new KnjigaAutorMapper(), idKnjigeAutora.toString());
    }

    @Override
    public List<KnjigaAutor> findAll() {
        String sql = "SELECT * FROM knjiga_autor";
        return jdbcTemplate.query(sql, new KnjigaAutorMapper());
    }

    @Override
    public List<KnjigaAutor> findByKnjiga(UUID idKnjige) {
        String sql = "SELECT * FROM knjiga_autor WHERE idKnjige = ?";

        return jdbcTemplate.query(sql, new KnjigaAutorMapper(), idKnjige.toString());
    }

    @Override
    public List<KnjigaAutor> findByAutor(UUID idAutora) {
        String sql = "SELECT * FROM knjiga_autor WHERE idAutora = ?";
        return jdbcTemplate.query(sql, new KnjigaAutorMapper(), idAutora.toString());
    }

    @Override
    public KnjigaAutor findByKnjigaAutor(UUID idKnjige, UUID idAutora) {
        String sql = "SELECT * FROM knjiga_autor WHERE idKnjige = ? AND idAutora = ?";
        return jdbcTemplate.queryForObject(sql, new KnjigaAutorMapper(), idKnjige.toString(), idAutora.toString());
    }

    @Transactional
    @Override
    public int save(KnjigaAutor knjigaAutor) {
        String sql = "INSERT INTO knjiga_autor VALUES (?,?,?)";
        return jdbcTemplate.update(sql, knjigaAutor.getIdKnjigeAutora().toString(), knjigaAutor.getKnjiga().getIdKnjige().toString(),
                knjigaAutor.getAutor().getIdAutora().toString());
    }

    @Transactional
    @Override
    public int update(KnjigaAutor knjigaAutor) {
        String sql = "UPDATE knjiga_autor SET idKnjige = ?, idAutora = ? WHERE idKnjigeAutora = ?";
        return jdbcTemplate.update(sql, knjigaAutor.getKnjiga().getIdKnjige().toString(), knjigaAutor.getAutor().getIdAutora().toString(),
                knjigaAutor.getIdKnjigeAutora().toString());
    }

    @Transactional
    @Override
    public int delete(UUID idKnjigeAutora) {
        String sql = "DELETE FROM knjiga_autor WHERE idKnjigeAutora = ?";
        return jdbcTemplate.update(sql, idKnjigeAutora.toString());
    }
}
