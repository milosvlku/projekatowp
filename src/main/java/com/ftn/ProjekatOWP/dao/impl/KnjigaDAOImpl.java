package com.ftn.ProjekatOWP.dao.impl;

import com.ftn.ProjekatOWP.dao.KnjigaDAO;
import com.ftn.ProjekatOWP.dao.ZanrDAO;
import com.ftn.ProjekatOWP.enums.Pismo;
import com.ftn.ProjekatOWP.enums.Povez;
import com.ftn.ProjekatOWP.models.Autor;
import com.ftn.ProjekatOWP.models.Knjiga;
import com.ftn.ProjekatOWP.models.Zanr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
public class KnjigaDAOImpl implements KnjigaDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private ZanrDAO zanrDAO;

    private class KnjigaRowMapper implements RowMapper<Knjiga> {

        @Override
        public Knjiga mapRow(ResultSet resultSet, int i) throws SQLException {
            int index = 1;
            UUID idKnjige = UUID.fromString(resultSet.getString(index++));
            String naziv = resultSet.getString(index++);
            String ISBN = resultSet.getString(index++);
            String izdavackaKuca = resultSet.getString(index++);
            int godinaIzdavanja = resultSet.getInt(index++);
            String kratakOpis = resultSet.getString(index++);
            String slika = resultSet.getString(index++);
            Double cena = resultSet.getDouble(index++);
            int brojStranica = resultSet.getInt(index++);
            Povez povez = Povez.valueOf(resultSet.getString(index++));
            Pismo pismo = Pismo.valueOf(resultSet.getString(index++));
            String jezik = resultSet.getString(index++);
            int brojPrimeraka = resultSet.getInt(index++);

            Knjiga knjiga = new Knjiga(idKnjige, naziv, ISBN, izdavackaKuca, godinaIzdavanja, kratakOpis, slika, cena, brojStranica,
                    povez, pismo, jezik, brojPrimeraka);

            return knjiga;
        }
    }

    @Override
    public Knjiga findOne(UUID idKnjige) {
        String sql = "SELECT * FROM knjiga WHERE idKnjige = ?";
        return jdbcTemplate.queryForObject(sql, new KnjigaRowMapper(), idKnjige.toString());
    }

    @Override
    public List<Knjiga> findAll() {
        String sql = "SELECT * FROM knjiga";
        return jdbcTemplate.query(sql, new KnjigaRowMapper());
    }

    @Transactional
    @Override
    public int save(Knjiga knjiga) {
        String sql = "INSERT INTO knjiga VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
        return jdbcTemplate.update(sql, knjiga.getIdKnjige().toString(), knjiga.getNaziv(), knjiga.getISBN(), knjiga.getIzdavackaKuca(),
                knjiga.getGodinaIzdavanja(), knjiga.getKratakOpis(), knjiga.getSlika(), knjiga.getCena(), knjiga.getBrojStranica(),
                knjiga.getPovez().toString(), knjiga.getPismo().toString(), knjiga.getJezik(), knjiga.getBrojPrimeraka());
    }

    @Transactional
    @Override
    public int update(Knjiga knjiga) {

        String sql = "DELETE FROM knjiga_zanr WHERE idKnjige = ?";
        jdbcTemplate.update(sql, knjiga.getIdKnjige().toString());

        boolean uspeh = true;
        sql = "INSERT INTO knjiga_zanr VALUES (?,?,?)";
        for(Zanr zanr:knjiga.getZanrovi()) {
            System.out.println("Knjiga " + knjiga.getIdKnjige().toString() + " Zanr" + zanr.getIdZanra().toString());
            uspeh = uspeh && jdbcTemplate.update(sql, UUID.randomUUID().toString(), knjiga.getIdKnjige().toString(), zanr.getIdZanra().toString()) == 1;
        }

        sql = "DELETE FROM knjiga_autor WHERE idKnjige = ?";
        jdbcTemplate.update(sql, knjiga.getIdKnjige().toString());

        sql = "INSERT INTO knjiga_autor VALUES (?,?,?)";
        for(Autor autor:knjiga.getAutori()) {
            System.out.println("Knjiga " + knjiga.getIdKnjige().toString() + " Autor " + autor.getIdAutora().toString());
            uspeh = uspeh && jdbcTemplate.update(sql, UUID.randomUUID().toString(), knjiga.getIdKnjige().toString(), autor.getIdAutora().toString()) == 1;
        }

        sql = "UPDATE knjiga SET naziv = ?, ISBN = ?, izdavackaKuca = ?, godinaIzdavanja = ?, kratakOpis = ?, slika = ?, cena = ?, brojStranica = ?, povez = ?, pismo = ?, jezik = ?, brojPrimeraka = ? WHERE idKnjige = ?";

        uspeh = uspeh & jdbcTemplate.update(sql, knjiga.getNaziv(), knjiga.getISBN(), knjiga.getIzdavackaKuca(), knjiga.getGodinaIzdavanja(),
                knjiga.getKratakOpis(), knjiga.getSlika(), knjiga.getCena(), knjiga.getBrojStranica(), knjiga.getPovez().toString(),
                knjiga.getPismo().toString(), knjiga.getJezik(), knjiga.getBrojPrimeraka(), knjiga.getIdKnjige().toString()) == 1;

        return uspeh?1:0;
    }

    @Transactional
    @Override
    public int delete(UUID idKnjige) {
        String sql = "DELETE FROM knjiga_zanr WHERE idKnjige = ?";
        int brojObrisanihKnjigaZanra = jdbcTemplate.update(sql, idKnjige.toString());
        System.out.println("Broj obrisanik knjiga_zanra = " + brojObrisanihKnjigaZanra);

        sql = "DELETE FROM knjiga_autor WHERE idKnjige = ?";
        int brojObrisanihKnjigaAutora = jdbcTemplate.update(sql, idKnjige.toString());
        System.out.println("Broj obrisanik knjiga_autora = " + brojObrisanihKnjigaAutora);

        sql = "DELETE FROM knjiga WHERE idKnjige = ?";
        return jdbcTemplate.update(sql, idKnjige.toString());
    }

    @Override
    public List<Knjiga> find(String naziv, String ISBN, Double cenaOd, Double cenaDo, String jezik) {
        ArrayList<Object> listaArgumenata = new ArrayList<>();
        System.out.println("Jezik " + jezik);

        String sql = "SELECT * FROM knjiga ";

        StringBuffer whereSql = new StringBuffer(" WHERE ");
        boolean imaArgumenata = false;

        if(naziv != null) {
            naziv = "%" + naziv + "%";
            if(imaArgumenata) {
                whereSql.append(" AND ");
            }
            whereSql.append("naziv LIKE ?");
            imaArgumenata = true;
            listaArgumenata.add(naziv);
        }

        if(ISBN != null) {
            if(imaArgumenata) {
                whereSql.append(" AND ");
            }
            whereSql.append("ISBN = ?");
            imaArgumenata = true;
            listaArgumenata.add(ISBN);
        }

        if(cenaOd != null) {
            if(imaArgumenata) {
                whereSql.append(" AND ");
            }
            whereSql.append("cena >= ?");
            imaArgumenata = true;
            listaArgumenata.add(cenaOd);
        }

        if(cenaDo != null) {
            if(imaArgumenata) {
                whereSql.append(" AND ");
            }
            whereSql.append("cena <= ?");
            imaArgumenata = true;
            listaArgumenata.add(cenaDo);
        }

        System.out.println("Jebani jezik = " +jezik);
        if(jezik != null) {
            if(imaArgumenata) {
                whereSql.append(" AND ");
            }
            whereSql.append("jezik = ?");
            imaArgumenata = true;
            listaArgumenata.add(jezik);
        }

        if(imaArgumenata) {
            sql = sql + whereSql.toString() + " ORDER BY idKnjige";
            System.out.println(sql);
        } else {
            sql = sql + " ORDER BY idKnjige";
            System.out.println(sql);
        }

        List<Knjiga> knjige = jdbcTemplate.query(sql, listaArgumenata.toArray(), new KnjigaRowMapper());

        return knjige;
    }


}
