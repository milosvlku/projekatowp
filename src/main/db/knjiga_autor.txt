CREATE TABLE `bazaknjizare`.`knjiga_autor` (
  `idKnjigeAutora` CHAR(36) NOT NULL,
  `idKnjige` CHAR(36) NOT NULL,
  `idAutora` CHAR(36) NOT NULL,
  PRIMARY KEY (`idKnjigeAutora`),
  INDEX `idKnjige_idx` (`idKnjige` ASC) VISIBLE,
  INDEX `idAutora_idx` (`idAutora` ASC) VISIBLE,
  CONSTRAINT `idKnjige`
    FOREIGN KEY (`idKnjige`)
    REFERENCES `bazaknjizare`.`knjiga` (`idKnjige`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `idAutora`
    FOREIGN KEY (`idAutora`)
    REFERENCES `bazaknjizare`.`autor` (`idAutora`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);
