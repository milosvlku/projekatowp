CREATE TABLE `bazaknjizare`.`loyalty_kartica` (
  `idLoyaltyKartice` CHAR(36) NOT NULL,
  `korisnik` CHAR(36) NOT NULL,
  `brojPoena` CHAR(36) NOT NULL,
  `status` ENUM('NA_CEKANJU', 'ODOBREN', 'NIJE_ODOBREN') NOT NULL,
  PRIMARY KEY (`idLoyaltyKartice`),
  INDEX `korisnik_idx` (`korisnik` ASC) VISIBLE,
  CONSTRAINT `korisnik2`
    FOREIGN KEY (`korisnik`)
    REFERENCES `bazaknjizare`.`korisnik` (`idKorisnika`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);
