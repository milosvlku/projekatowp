CREATE TABLE `bazaknjizare`.`autor` (
  `idAutora` CHAR(36) NOT NULL,
  `imeAutora` VARCHAR(45) NOT NULL,
  `prezimeAutora` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idAutora`));
