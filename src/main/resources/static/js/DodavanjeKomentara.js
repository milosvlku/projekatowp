var zvezdica1 = document.getElementById('zvezdica1');
var zvezdica2 = document.getElementById('zvezdica2');
var zvezdica3 = document.getElementById('zvezdica3');
var zvezdica4 = document.getElementById('zvezdica4');
var zvezdica5 = document.getElementById('zvezdica5');
var ocena = document.getElementById('ocena');
var submit = document.getElementById('submit');
var unosKomentara = document.getElementById('unos');


zvezdica1.addEventListener('mouseover', function () {
    zvezdica1.classList.add("checked");
});

zvezdica2.addEventListener('mouseover', function () {
    zvezdica1.classList.add("checked");
    zvezdica2.classList.add("checked");
});

zvezdica3.addEventListener('mouseover', function () {
    zvezdica1.classList.add("checked");
    zvezdica2.classList.add("checked");
    zvezdica3.classList.add("checked");
});

zvezdica4.addEventListener('mouseover', function () {
    zvezdica1.classList.add("checked");
    zvezdica2.classList.add("checked");
    zvezdica3.classList.add("checked");
    zvezdica4.classList.add("checked");
});

zvezdica5.addEventListener('mouseover', function () {
    zvezdica1.classList.add("checked");
    zvezdica2.classList.add("checked");
    zvezdica3.classList.add("checked");
    zvezdica4.classList.add("checked");
    zvezdica5.classList.add("checked");
});


zvezdica1.addEventListener('mouseout', function () {
    deselektujSveZvezdice();
});

zvezdica2.addEventListener('mouseout', function () {
    deselektujSveZvezdice();
});

zvezdica3.addEventListener('mouseout', function () {
    deselektujSveZvezdice();
});

zvezdica4.addEventListener('mouseout', function () {
    deselektujSveZvezdice();
});

zvezdica5.addEventListener('mouseout', function () {
    deselektujSveZvezdice();
});

function deselektujSveZvezdice() {
    zvezdica1.classList.remove("checked");
    zvezdica2.classList.remove("checked");
    zvezdica3.classList.remove("checked");
    zvezdica4.classList.remove("checked");
    zvezdica5.classList.remove("checked");
}

zvezdica1.addEventListener('click', function () {
    deIzaberiSveZvezdice();
    zvezdica1.classList.add("izabrano");
    ocena.value = 1;
});

zvezdica2.addEventListener('click', function () {
    deIzaberiSveZvezdice();
    zvezdica1.classList.add("izabrano");
    zvezdica2.classList.add("izabrano");
    ocena.value = 2;
});

zvezdica3.addEventListener('click', function () {
    deIzaberiSveZvezdice();
    zvezdica1.classList.add("izabrano");
    zvezdica2.classList.add("izabrano");
    zvezdica3.classList.add("izabrano");
    ocena.value = 3;
});

zvezdica4.addEventListener('click', function () {
    deIzaberiSveZvezdice();
    zvezdica1.classList.add("izabrano");
    zvezdica2.classList.add("izabrano");
    zvezdica3.classList.add("izabrano");
    zvezdica4.classList.add("izabrano");
    ocena.value = 4;
});

zvezdica5.addEventListener('click', function () {
    deIzaberiSveZvezdice();
    zvezdica1.classList.add("izabrano");
    zvezdica2.classList.add("izabrano");
    zvezdica3.classList.add("izabrano");
    zvezdica4.classList.add("izabrano");
    zvezdica5.classList.add("izabrano");
    ocena.value = 5;
});

function deIzaberiSveZvezdice() {
    zvezdica1.classList.remove("izabrano");
    zvezdica2.classList.remove("izabrano");
    zvezdica3.classList.remove("izabrano");
    zvezdica4.classList.remove("izabrano");
    zvezdica5.classList.remove("izabrano");
}

submit.addEventListener('click', function (e){
    if(ocena.value != 0 && unosKomentara.value.length > 5) {
        //nastavi
    } else {
        e.preventDefault();
        window.alert("Popunite tekst za komentar, i dajte ocenu od 1 do 5.");
    }
}, false);