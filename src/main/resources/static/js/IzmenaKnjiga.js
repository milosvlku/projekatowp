var knjigeURL = 'https://kontrolnatacka-74a7b-default-rtdb.firebaseio.com/knjige.json';
var request = new XMLHttpRequest();
var izmenaForm = document.getElementById('izmenaForm');



//Ovde se salje forma
izmenaForm.addEventListener('submit', async function(e){

    var porukaOGresci = "";

    var naziv = document.getElementById('naziv').value.trim();
    console.log(naziv);

    if(naziv == '' || naziv == null) {
        porukaOGresci += "Naziv ne sme biti izostavljen";
    } if(isNumeric(naziv)) {
        porukaOGresci += ", Naziv ne sme biti broj!";
    }

    var isbn = document.getElementById('isbn').value;
    console.log(isbn);

    //izdavackaKuca
    var izdavackaKuca = document.getElementById('izdavackaKuca').value;
    
    if(izdavackaKuca == '' || izdavackaKuca == null) {
        porukaOGresci += ", Izdavacka Kuca ne sme biti izostavljena";
    } if(isNumeric(izdavackaKuca)) {
        porukaOGresci += ", Izdavacka Kuca ne sme biti broj!";
    }

    //autor
    var autor = document.getElementById('autor').value;
    if(autor == '' || autor == null) {
        porukaOGresci += ", Autor ne sme biti izostavljen";
    } if(isNumeric(autor)) {
        porukaOGresci += ", Autor ne sme biti broj!";
    }

    //godina
    var godina = document.getElementById('godina').value;
    if(godina == '' || godina == null) {
        porukaOGresci += ", Godina ne sme biti izostavljena";
    } else if(godina <= 0) {
        porukaOGresci += ", Godina ne sme biti manja od 0";
    }

    //cena
    var cena = document.getElementById('cena').value;
    if(cena == '' || cena == null) {
        porukaOGresci += ", Cena ne sme biti izostavljena";
    } else if(godina <= 0) {
        porukaOGresci += ", Cena ne sme biti manja od 0";
    }

    //brojStranica
    var brojStranica = document.getElementById('brojStranica').value;
    if(brojStranica == '' || brojStranica == null) {
        porukaOGresci += ", Broj stranica ne sme biti izostavljena";
    } else if(godina <= 0) {
        porukaOGresci += ", Broj stranica ne sme biti manja od 0";
    }

    //povez
    var povez = document.getElementById('povez').value;
    console.log(povez);

    //pismo
    var pismo = document.getElementById('pismo').value;
    console.log(pismo);

    //jezik
    var jezik = document.getElementById('jezik').value;
    if(jezik == '' || jezik == null) {
        porukaOGresci += ", Jezik ne sme biti izostavljen";
    } if(isNumeric(jezik)) {
        porukaOGresci += ", Jezik ne sme biti broj!";
    }

    if(porukaOGresci == '') {

    } else {
        e.preventDefault();
        alert('Greska! ' + porukaOGresci);
    }

});



function vratiSelektovanPovez(povez) {
    var broj = 0;
    if(povez == "Meki") {
        broj = 2;
    } else if(povez == 'Tvrdi') {
        broj = 1;
    }
    return broj;
}

function vratiSelektovanoPismo(pismo) {
    var broj = 0;
    if(pismo == "Latinica") {
        broj = 1;
    } else if(povez == 'Ćirilica') {
        pismo = 2;
    }
    return broj;
}

function vratiIzdavackeKuce() {

    var izdavackeKuce = [];

    return new Promise(function(resolve) {
        request.onreadystatechange = function() {
            if(this.readyState == 4) {
                if(this.status == 200) {
                    var knjige = JSON.parse(this.responseText);
                    
    
                    // DEFER
    
                    for(var i = 0; i < Object.keys(knjige).length; i++) {
                        
                        //Cupa pojedinacnu knjigu tj pojedinacan info o knjizi
                        var knjiga = knjige[Object.keys(knjige)[i]];
                        izdavackeKuce.push(knjiga.izdavackaKuca.toLowerCase());
                        
                    }
                    console.log("Doslo do toga!");
                    console.log(izdavackeKuce);
                    resolve(izdavackeKuce);
                } else {
                    alert('Greska prilikom ucitavanja podataka o knjigama.');
                }
            }   
        };
    
        request.open('GET', knjigeURL);
        request.send();
    });

    
}

function isNumeric(str) {
    if (typeof str != "string") return false
    return !isNaN(str) && 
           !isNaN(parseFloat(str))
}