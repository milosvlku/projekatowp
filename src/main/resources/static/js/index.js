var knjigeURL = 'https://kontrolnatacka-74a7b-default-rtdb.firebaseio.com/knjige.json';
var korisniciURL = 'https://kontrolnatacka-74a7b-default-rtdb.firebaseio.com/korisnici.json';
var tabela = document.getElementById('tabela');
var request = new XMLHttpRequest();
var loginForm = document.getElementById('loginForm');
var lupa = document.getElementById('pretragaIkonica');
var dugmeRegistujSe = document.getElementById('dugmeRegistrujSe');
var sortiraj = document.getElementById('sortiranjeIkonica');
var sortirajPoNazivu = document.getElementById('sortirajPoNazivu');
var sortirajPoCeni = document.getElementById('sortirajPoCeni');
var sortirajPoAutoru = document.getElementById('sortirajPoAutoru');
var sortirajPoJeziku = document.getElementById('sortirajPoJeziku');
var sortirajPoProsecnojOceni = document.getElementById('sortirajPoProsecnojOceni');

// request.onreadystatechange = function() {
//     if(this.readyState == 4) {
//         if(this.status == 200) {
//             var knjige = JSON.parse(this.responseText);
//             console.log(Object.keys(knjige));
//             for(var i = 0; i < Object.keys(knjige).length; i++) {
//                 console.log(i);
//                 //Cupa pojedinacnu knjigu tj pojedinacan info o knjizi
//                 var knjiga = knjige[Object.keys(knjige)[i]];
                

//                 //Kreira pojedinacan tr za knjigu
//                 var knjigaTr = document.createElement('tr');
                
//                 //Kreira pojedinacne td za knjigu
//                 //Za ime knjige i link do knjige
//                 var imeKnjigeTD = document.createElement('td');
//                 var linkDoKnjige = document.createElement('a');
//                 linkDoKnjige.setAttribute('href', knjiga.naziv);
//                 linkDoKnjige.innerText = knjiga.naziv;
//                 imeKnjigeTD.appendChild(linkDoKnjige);
//                 knjigaTr.appendChild(imeKnjigeTD);
                

//                 //Za autora knjige
//                 var autorKnjigeTD = document.createElement('td');
//                 autorKnjigeTD.innerText = knjiga.autor;
//                 knjigaTr.appendChild(autorKnjigeTD);

//                 //Za godinu knjige
//                 var godinaIzdavanjaTD = document.createElement('td');
//                 godinaIzdavanjaTD.innerText = knjiga.godinaIzdavanja;
//                 knjigaTr.appendChild(godinaIzdavanjaTD);

//                 //Za cenu knjige
//                 var cenaKnjigeTD = document.createElement('td');
//                 cenaKnjigeTD.innerText = knjiga.cena;
//                 knjigaTr.appendChild(cenaKnjigeTD);

//                 //Za ocenu knjige
//                 var ocenaKnjifeTD = document.createElement('td');
//                 ocenaKnjifeTD.innerText = knjiga.ocena;
//                 knjigaTr.appendChild(ocenaKnjifeTD);

//                 tabela.appendChild(knjigaTr);
//             }

//         } else {
//             alert('Greska prilikom ucitavanja podataka o knjigama.');
//         }
//     }
// }

// request.open('GET', knjigeURL);
// request.send();

try {
    document.getElementById('poslednji').addEventListener('click', function() {
        document.querySelector('.loginBackground').style.display = 'flex';
    });
} catch (err) {
    document.getElementById('odjava').addEventListener('click', function() {
        //Ovu funkcionalnost implementira Thymeleaf u basePage-u.
    });
}

//funkcionalnost ikonice sortiraj
sortiraj.addEventListener('mouseover', function (e) {
    document.getElementById('subnav').style.display = 'block';
});

sortiraj.addEventListener('mouseleave', function (e) {
    document.getElementById('subnav').style.display = 'none';
});

//funkcionalnost sortiranja po nazivu
sortirajPoNazivu.addEventListener('mouseover', function (e) {
    document.getElementById('sortirajPoNazivuSubNav').style.display = 'block';
});

sortirajPoNazivu.addEventListener('mouseleave', function (e) {
    document.getElementById('sortirajPoNazivuSubNav').style.display = 'none';
});

//funkcionalnost sortiranja po ceni
sortirajPoCeni.addEventListener('mouseover', function (e) {
    document.getElementById('sortirajPoCeniSubNav').style.display = 'block';
});

sortirajPoCeni.addEventListener('mouseleave', function (e) {
    document.getElementById('sortirajPoCeniSubNav').style.display = 'none';
});

//funkcionalnost sortiranja po autoru
sortirajPoAutoru.addEventListener('mouseover', function (e) {
    document.getElementById('sortirajPoAutoruSubNav').style.display = 'block';
});

sortirajPoAutoru.addEventListener('mouseleave', function (e) {
    document.getElementById('sortirajPoAutoruSubNav').style.display = 'none';
});

//funkcionalnost sortiranja po Jeziku
sortirajPoJeziku.addEventListener('mouseover', function (e) {
    document.getElementById('sortirajPoJezikuSubNav').style.display = 'block';
});

sortirajPoJeziku.addEventListener('mouseleave', function (e) {
    document.getElementById('sortirajPoJezikuSubNav').style.display = 'none';
});

//funkcionalnost sortiranja po Prosecnoj Oceni
sortirajPoProsecnojOceni.addEventListener('mouseover', function (e) {
    document.getElementById('sortirajPoProsecnojOceniSubNav').style.display = 'block';
});

sortirajPoProsecnojOceni.addEventListener('mouseleave', function (e) {
    document.getElementById('sortirajPoProsecnojOceniSubNav').style.display = 'none';
});

lupa.addEventListener('click', function(e) {
    if(document.querySelector('#pretragaDiv').style.display == 'block') {
        document.querySelector('#pretragaDiv').style.display = 'none';
    } else {
        document.querySelector('#pretragaDiv').style.display = 'block';
    }
});

dugmeRegistujSe.addEventListener('click', function () {
    window.location.href = '/Knjizara/Korisnici/Registracija';
});

document.querySelector('.close').addEventListener('click', function(){
    document.querySelector('.loginBackground').style.display = 'none';
});

// loginForm.addEventListener('submit', function(e) {
//     e.preventDefault();

//     var username = document.getElementById('txtUsername').value.trim();
//     var password = document.getElementById('txtPassword').value.trim();

//     if(username == '' || password == '') {
//         alert("Unesite sve kredencijale za korisnika!");
//     } else {
//         request.onreadystatechange = function() {
//             if(this.readyState == 4) {
//                 if(this.status == 200) {
//                     var korisnici = JSON.parse(this.responseText);
//                     console.log(Object.keys("Korisnici: " + korisnici));

//                     var uspesnoUlogovanje = false;

//                     for(var i = 0; i < Object.keys(korisnici).length; i++) {

//                         var korisnik = korisnici[Object.keys(korisnici)[i]];

//                         if(username.toUpperCase() == korisnik.username.toUpperCase() && password == korisnik.password) {
//                             uspesnoUlogovanje = true;
//                             break;
//                         }

//                     }

//                     if(uspesnoUlogovanje) {

//                         document.querySelector('.loginBackground').style.display = 'none';
//                         alert('Uspesno ulogovanje ' + username + " " + password);

//                     } else {
//                         alert('Pogresan username ili sifra');
//                     }
        
//                 } else {
//                     alert('Greska prilikom ucitavanja podataka o knjigama.');
//                 }
//             }
//         };
        
//         request.open('GET', korisniciURL);
//         request.send();
//     }

// });