var dugmeIzmeni = document.getElementById('dugmeIzmeni');
var dugmeKomentarisi = document.getElementById('dugmeKomentarisi');
var idKnjige = document.getElementById('idKnjige').value;
var dodajUListuZeljaIkonica = document.getElementById('dodajUListuZelja');
var dodajIzbaciIzListeZeljaForm = document.getElementById('uListuZeljaForm');
var dodajUKorpuForm = document.getElementById('uKorpuForm');
var dugmeDodajUKorpu = document.getElementById('dugmeDodajUKorpu');

//Dugme izmeni ce biti prisutno samo ako je ulogovan admin.
if(dugmeIzmeni !== null) {
    dugmeIzmeni.addEventListener('click', function (e) {
        window.location.href = "Izmeni?idKnjige=" + idKnjige;
    });
}

if(dugmeDodajUKorpu !== null) {
    dugmeDodajUKorpu.addEventListener('click', function (e) {
        dodajUKorpuForm.submit();
    });
}

if(dugmeKomentarisi !== null) {
    dugmeKomentarisi.addEventListener('click', function (e) {
        window.location.href = "DodajKomentar?idKnjige=" + idKnjige;
    });
}

if(dodajUListuZeljaIkonica !== null) {
    dodajUListuZeljaIkonica.addEventListener('click', function (e) {
        console.log(dodajIzbaciIzListeZeljaForm == null)
        dodajIzbaciIzListeZeljaForm.submit();


    });
}

$(document).ready(function () {
    var baseURL = "http://localhost:8080/Knjizara";
    var currentURL = window.location.href;
    console.log("Test")
    console.log(baseURL+"/Komentari");



    function komentari() {
        $.get(baseURL + "/Komentari", function (odgovor) {
            let knjiga = new URLSearchParams(window.location.search);
            var idKnj = knjiga.get('idKnjige');
            console.log(idKnj)
            var htmlText = "";
            var komentari = odgovor.komentari;
            console.log(komentari)
            for(var itKomentar in komentari) {
                if(komentari[itKomentar].status == "ODOBREN" && idKnj == komentari[itKomentar].knjiga.idKnjige) {
                    htmlText += "<strong>" + komentari[itKomentar].autorKomentara.korisnickoIme + "</strong>";
                    htmlText += "<p>" + komentari[itKomentar].tekstKomentara + "</p>";
                }
            }
            $("#komentari").html(htmlText);
        })
    }

    komentari()
})





